(function ($) {
  'use strict';
  Drupal.behaviors.multilingualAuditReport = {
    attach: function (context) {
      // When the Dismiss link is clicked hide the row.
      $('a[data-audit-dismiss]').click(function (event) {
        event.preventDefault();
        $(this).parent().closest('tr').fadeOut('slow', function () {
          $(this).remove();
        });
      });
    }
  };
}(jQuery));
