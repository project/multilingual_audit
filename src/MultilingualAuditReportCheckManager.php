<?php

namespace Drupal\multilingual_audit;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Provides an MultilingualAuditReportCheck plugin manager.
 *
 * @see \Drupal\multilingual_audit\Annotation\MultilingualAuditReportCheck
 * @see plugin_api
 */
class MultilingualAuditReportCheckManager extends DefaultPluginManager {

  /**
   * Constructs a MultilingualAuditReportCheckManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/MultilingualAuditReportCheck',
      $namespaces,
      $module_handler,
      '\Drupal\multilingual_audit\MultilingualAuditReportCheckInterface',
      '\Drupal\multilingual_audit\Annotation\MultilingualAuditReportCheck'
    );
    $this->alterInfo('multilingual_audit_report_check_info');
    $this->setCacheBackend($cache_backend, 'multilingual_audit_report_check_info_plugins');
  }

}
