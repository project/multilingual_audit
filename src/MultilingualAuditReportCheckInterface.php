<?php


namespace Drupal\multilingual_audit;


interface MultilingualAuditReportCheckInterface {

  /**
   * Checks if the requirements are met for running this check.
   *
   * @return bool
   */
  public function checkRequirements();

  /**
   * Runs the report check.
   *
   * @return \Drupal\multilingual_audit\MultilingualAuditReportCheckResult[]
   *   The results.
   */
  public function run();

}
