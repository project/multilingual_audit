<?php

namespace Drupal\multilingual_audit\Plugin\MultilingualAuditReportCheck;

use Drupal\Component\Plugin\PluginBase;
use Drupal\content_translation\ContentTranslationManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Link;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\lingotek\LingotekConfigurationServiceInterface;
use Drupal\multilingual_audit\MultilingualAuditReportCheckInterface;
use Drupal\multilingual_audit\MultilingualAuditReportCheckResult;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @MultilingualAuditReportCheck (
 *   id = "paragraphs_translatable_checker",
 *   title = @Translation("Paragraphs translatable check"),
 *   description = @Translation("Checks the paragraphs are translatable."),
 *   weight = 35,
 * )
 */
class ParagraphsTranslatableChecker extends PluginBase implements MultilingualAuditReportCheckInterface, ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The content translation manager.
   *
   * @var \Drupal\content_translation\ContentTranslationManagerInterface
   */
  protected $contentTranslationManager;

  /**
   * The Lingotek configuration service.
   *
   * @var \Drupal\lingotek\LingotekConfigurationServiceInterface
   */
  protected $lingotekConfiguration;

  /**
   * MultilingualModulesChecker constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\content_translation\ContentTranslationManagerInterface|NULL $content_translation_manager
   *   The content translation manager.
   * @param \Drupal\lingotek\LingotekConfigurationServiceInterface|NULL $lingotek_configuration
   *   The Lingotek configuration service.
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition, ModuleHandlerInterface $module_handler, EntityTypeManagerInterface $entity_type_manager, ContentTranslationManagerInterface $content_translation_manager = NULL, LingotekConfigurationServiceInterface $lingotek_configuration = NULL) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->moduleHandler = $module_handler;
    $this->entityTypeManager = $entity_type_manager;
    $this->contentTranslationManager = $content_translation_manager;
    $this->lingotekConfiguration = $lingotek_configuration;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $contentTranslationManager = $container->has('content_translation.manager') ? $container->get('content_translation.manager') : NULL;
    $lingotekConfiguration = $container->has('lingotek.configuration') ? $container->get('lingotek.configuration') : NULL;
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('module_handler'),
      $container->get('entity_type.manager'),
      $contentTranslationManager,
      $lingotekConfiguration
    );
  }

  /**
   * {@inheritdoc}
   */
  public function checkRequirements() {
    return ($this->moduleHandler->moduleExists('content_translation') && $this->moduleHandler->moduleExists('paragraphs'));
  }

  /**
   * {@inheritdoc}
   */
  public function run() {
    $results = [];

    $paragraphsTypes = $this->entityTypeManager->getStorage('paragraphs_type')->loadMultiple();
    if ($paragraphsTypes) {
      foreach ($paragraphsTypes as $paragraphsType) {
        $enabled = $this->contentTranslationManager->isEnabled('paragraph', $paragraphsType->id());
        if (!$enabled) {
          $message = $this->t('The paragraph type %paragraphType is not enabled for content translation.', [
            '%paragraphType' => $paragraphsType->label(),
          ]);
          $action = Link::createFromRoute($this->t('Enable content translatability if desired.'), 'language.content_settings_page');
          $result = new MultilingualAuditReportCheckResult();
          $result->setMessage($message)
            ->addSuggestedAction($action)
            ->setStatus('warning');
        }
        else {
          $message = $this->t('The paragraph type %paragraphType is enabled for content translation.', [
            '%paragraphType' => $paragraphsType->label(),
          ]);
          $result = new MultilingualAuditReportCheckResult();
          $result->setMessage($message)
            ->setStatus('ok');

          if ($this->moduleHandler->moduleExists('lingotek')) {
            $lingotekEnabled = $this->lingotekConfiguration->isEnabled('paragraph', $paragraphsType->id());
            if (!$lingotekEnabled) {
              $message = $this->t('The paragraph type %paragraphType is not enabled for Lingotek translation.', [
                '%paragraphType' => $paragraphsType->label(),
              ]);
              $action = Link::createFromRoute($this->t('Enable Lingotek translatability if desired.'), 'lingotek.settings.content_form', ['entity_type' => 'paragraph', 'bundle' => $paragraphsType->id()]);
              $result = new MultilingualAuditReportCheckResult();
              $result->setMessage($message)
                ->addSuggestedAction($action)
                ->setStatus('warning');
            }
          }
        }
        $results[] = $result;
      }
    }
    return $results;
  }

}
