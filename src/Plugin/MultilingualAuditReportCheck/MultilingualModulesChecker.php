<?php

namespace Drupal\multilingual_audit\Plugin\MultilingualAuditReportCheck;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Link;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\multilingual_audit\MultilingualAuditReportCheckInterface;
use Drupal\multilingual_audit\MultilingualAuditReportCheckResult;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @MultilingualAuditReportCheck (
 *   id = "multilingual_modules_checker",
 *   title = @Translation("Multilingual modules check"),
 *   description = @Translation("Checks that the multilingual modules are enabled on your site."),
 *   weight = 0,
 * )
 */
class MultilingualModulesChecker extends PluginBase implements MultilingualAuditReportCheckInterface, ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * MultilingualModulesChecker constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition, ModuleHandlerInterface $module_handler) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function checkRequirements() {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function run() {
    $results = [];

    $requiredModules = ['language', 'content_translation'];
    $recommendedModules = ['lingotek'];
    foreach ($requiredModules as $requiredModule) {
      if (!$this->moduleHandler->moduleExists($requiredModule)) {
        $message = $this->t('Module %module is not enabled.', ['%module' => $requiredModule]);
        $action = Link::createFromRoute($this->t('Enable %module module.', ['%module' => $requiredModule]), 'system.modules_list');
        $result = new MultilingualAuditReportCheckResult();
        $result->setMessage($message)
          ->addSuggestedAction($action)
          ->setStatus('error');
      }
      else {
        $message = $this->t('Module %module is enabled.', ['%module' => $requiredModule]);
        $result = new MultilingualAuditReportCheckResult();
        $result->setMessage($message)
          ->setStatus('ok');
      }
      $results[] = $result;
    }
    foreach ($recommendedModules as $recommendedModule) {
      if (!$this->moduleHandler->moduleExists($recommendedModule)) {
        $message = $this->t('Module %module is not enabled.', ['%module' => $recommendedModule]);
        $action = Link::createFromRoute($this->t('Enable %module module.', ['%module' => $recommendedModule]), 'system.modules_list');
        $result = new MultilingualAuditReportCheckResult();
        $result->setMessage($message)
          ->addSuggestedAction($action)
          ->setStatus('warning');
      }
      else {
        $message = $this->t('Module %module is enabled.', ['%module' => $recommendedModule]);
        $result = new MultilingualAuditReportCheckResult();
        $result->setMessage($message)
          ->setStatus('ok');
      }
      $results[] = $result;
    }

    return $results;
  }

}
