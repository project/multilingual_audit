<?php

namespace Drupal\multilingual_audit\Plugin\MultilingualAuditReportCheck;

use Drupal\Component\Plugin\PluginBase;
use Drupal\content_translation\ContentTranslationManagerInterface;
use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Field\FieldConfigInterface;
use Drupal\Core\Link;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\lingotek\LingotekConfigurationServiceInterface;
use Drupal\multilingual_audit\MultilingualAuditReportCheckInterface;
use Drupal\multilingual_audit\MultilingualAuditReportCheckResult;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @MultilingualAuditReportCheck (
 *   id = "entity_reference_field_translatable_checker",
 *   title = @Translation("Entity reference field translatable check"),
 *   description = @Translation("Checks the entity reference fields are translatable."),
 *   weight = 20,
 * )
 */
class EntityReferenceFieldTranslatableChecker extends PluginBase implements MultilingualAuditReportCheckInterface, ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity type bundle info.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The content translation manager.
   *
   * @var \Drupal\content_translation\ContentTranslationManagerInterface
   */
  protected $contentTranslationManager;

  /**
   * The Lingotek configuration service.
   *
   * @var \Drupal\lingotek\LingotekConfigurationServiceInterface
   */
  protected $lingotekConfiguration;

  /**
   * MultilingualModulesChecker constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The bundle info service.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\content_translation\ContentTranslationManagerInterface|NULL $content_translation_manager
   *   The content translation manager.
   * @param \Drupal\lingotek\LingotekConfigurationServiceInterface|NULL $lingotek_configuration
   *   The Lingotek configuration service.
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition, ModuleHandlerInterface $module_handler, EntityTypeManagerInterface $entity_type_manager, EntityTypeBundleInfoInterface $entity_type_bundle_info, EntityFieldManagerInterface $entity_field_manager, ContentTranslationManagerInterface $content_translation_manager = NULL, LingotekConfigurationServiceInterface $lingotek_configuration = NULL) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->moduleHandler = $module_handler;
    $this->entityTypeManager = $entity_type_manager;
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
    $this->entityFieldManager = $entity_field_manager;
    $this->contentTranslationManager = $content_translation_manager;
    $this->lingotekConfiguration = $lingotek_configuration;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $contentTranslationManager = $container->has('content_translation.manager') ? $container->get('content_translation.manager') : NULL;
    $lingotekConfiguration = $container->has('lingotek.configuration') ? $container->get('lingotek.configuration') : NULL;
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('module_handler'),
      $container->get('entity_type.manager'),
      $container->get('entity_type.bundle.info'),
      $container->get('entity_field.manager'),
      $contentTranslationManager,
      $lingotekConfiguration
    );
  }

  /**
   * {@inheritdoc}
   */
  public function checkRequirements() {
    return ($this->moduleHandler->moduleExists('content_translation'));
  }

  /**
   * {@inheritdoc}
   */
  public function run() {
    $results = [];

    $entity_definitions = $this->entityTypeManager->getDefinitions();
    if ($entity_definitions) {
      foreach ($entity_definitions as $entity_id => $entity_definition) {
        if ($entity_definition instanceof ContentEntityTypeInterface && $entity_definition->hasKey('langcode')) {
          if ($this->contentTranslationManager->isSupported($entity_id)) {
            $bundle_info = $this->entityTypeBundleInfo->getBundleInfo($entity_definition->id());
            foreach ($bundle_info as $bundle_id => $bundle_definition) {
              if ($this->contentTranslationManager->isEnabled($entity_id, $bundle_id)) {
                /** @var \Drupal\Core\Field\FieldConfigInterface[] $fields */
                $fields = $this->entityFieldManager->getFieldDefinitions($entity_id, $bundle_id);

                foreach ($fields as $field_id => $field_definition) {
                  $lingotekAction = FALSE;
                  if ($field_definition->getType() === 'entity_reference' && !$field_definition->isReadOnly()) {
                    $contentTranslationActionText = $this->t('Review the translatability of this field.', [
                      '%entity' => $entity_definition->getLabel(),
                      '%bundle' => $bundle_definition['label'],
                      '%field' => $field_definition->getName(),
                    ]);
                    if ($field_definition instanceof FieldConfigInterface) {
                      $contentTranslationAction = Link::createFromRoute($contentTranslationActionText, "entity.field_config.{$entity_id}_field_edit_form", [
                        $entity_definition->getBundleEntityType() => $bundle_id,
                        'field_config' => $field_definition->id(),
                      ]);
                    }
                    else {
                      $contentTranslationAction = Link::createFromRoute($contentTranslationActionText, "language.content_settings_page");
                    }
                    if ($field_definition->isTranslatable()) {
                      $contentTranslationMessage = $this->t('The entity %entity bundle %bundle entity_reference field %field is enabled for content translation. That means you can reference different entities in each translation.', [
                        '%entity' => $entity_definition->getLabel(),
                        '%bundle' => $bundle_definition['label'],
                        '%field' => $field_definition->getName(),
                      ]);

                      if ($this->moduleHandler->moduleExists('lingotek') &&
                        $this->lingotekConfiguration->isEnabled($entity_id, $bundle_id) &&
                        $this->lingotekConfiguration->isFieldLingotekEnabled($entity_id, $bundle_id, $field_id)) {
                        $lingotekTranslationMessage = $this->t('The entity %entity bundle %bundle entity_reference field %field is enabled for Lingotek translation. That means the referenced content will be embedded with the parent document. You may only want that if this content is not used elsewhere.', [
                          '%entity' => $entity_definition->getLabel(),
                          '%bundle' => $bundle_definition['label'],
                          '%field' => $field_definition->getName(),
                        ]);
                        $lingotekAction = Link::createFromRoute($this->t('Review Lingotek translatability of this field.'), 'lingotek.settings.content_form', [
                          'entity_type' => $entity_id,
                          'bundle' => $bundle_id,
                        ]);
                      }
                    }
                    else {
                      $contentTranslationMessage = $this->t('The entity %entity bundle %bundle entity_reference field %field is not enabled for content translation. That means the field will reference the same entities in each translation.', [
                        '%entity' => $entity_definition->getLabel(),
                        '%bundle' => $bundle_definition['label'],
                        '%field' => $field_definition->getName(),
                      ]);
                    }
                    $contentTranslationResult = new MultilingualAuditReportCheckResult();
                    $contentTranslationResult->setMessage($contentTranslationMessage)
                      ->addSuggestedAction($contentTranslationAction)
                      ->setStatus('warning');
                    $results[] = $contentTranslationResult;
                    if ($lingotekAction) {
                      $lingotekTranslationResult = new MultilingualAuditReportCheckResult();
                      $lingotekTranslationResult->setMessage($lingotekTranslationMessage)
                        ->addSuggestedAction($lingotekAction)
                        ->setStatus('warning');
                      $results[] = $lingotekTranslationResult;
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    return $results;
  }

}
