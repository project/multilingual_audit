<?php

namespace Drupal\multilingual_audit\Plugin\MultilingualAuditReportCheck;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Link;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\lingotek\LanguageLocaleMapperInterface;
use Drupal\multilingual_audit\MultilingualAuditReportCheckInterface;
use Drupal\multilingual_audit\MultilingualAuditReportCheckResult;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @MultilingualAuditReportCheck (
 *   id = "languages_checker",
 *   title = @Translation("Languages check"),
 *   description = @Translation("Checks the languages enabled on your site."),
 *   weight = 5,
 * )
 */
class LanguagesChecker extends PluginBase implements MultilingualAuditReportCheckInterface, ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The language-locale mapper.
   *
   * @var \Drupal\lingotek\LanguageLocaleMapperInterface|NULL
   */
  protected $languageLocaleMapper;

  /**
   * LanguagesChecker constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\lingotek\LanguageLocaleMapperInterface|null $language_locale_mapper
   *   The Lingotek language-locale mapper if available.
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition, ModuleHandlerInterface $module_handler, LanguageManagerInterface $language_manager, EntityTypeManagerInterface $entity_type_manager, LanguageLocaleMapperInterface $language_locale_mapper = NULL) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->moduleHandler = $module_handler;
    $this->languageManager = $language_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->languageLocaleMapper = $language_locale_mapper;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $lingotekLanguageLocaleMapper = $container->has('lingotek.language_locale_mapper') ? $container->get('lingotek.language_locale_mapper') : NULL;
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('module_handler'),
      $container->get('language_manager'),
      $container->get('entity_type.manager'),
      $lingotekLanguageLocaleMapper
    );
  }

  public function checkRequirements() {
    return ($this->moduleHandler->moduleExists('language'));
  }

  public function run() {
    $results = [];

    $languages = $this->languageManager->getLanguages(LanguageInterface::STATE_CONFIGURABLE);
    if ($this->languageManager->isMultilingual()) {
      $languagesNames = array_map(function (LanguageInterface $language) { return $language->getName();}, $languages);
      $message = $this->t('The site has various languages: %languages', ['%languages' => implode(', ', $languagesNames)]);
      $result = new MultilingualAuditReportCheckResult();
      $result->setMessage($message)
        ->setStatus('ok');
      $results[] = $result;

      /** @var \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler */
      if ($this->moduleHandler->moduleExists('lingotek')) {
        foreach ($languages as $language) {
          $id = $language->getId();
          $configurableLanguage = $this->entityTypeManager->getStorage('configurable_language')->load($id);
          $explicitLocale = $configurableLanguage->getThirdPartySetting('lingotek', 'locale', NULL);
          $locale = $this->languageLocaleMapper->getLocaleForLangcode($language->getId());

          $result = new MultilingualAuditReportCheckResult();
          if ($explicitLocale) {
            $message = $this->t('The language %language maps to the %locale Lingotek locale.', [
              '%language' => $language->getName(),
              '%locale' => $locale,
            ]);
            $result->setStatus('info');
          }
          else {
            $message = $this->t("The language %language maps to the %locale Lingotek locale, but it's not set explicitly.", [
              '%language' => $language->getName(),
              '%locale' => $locale,
            ]);
            $action = Link::createFromRoute($this->t('Verify this is the desired Lingotek locale.'), 'entity.configurable_language.edit_form', ['configurable_language' => $language->getId()]);
            $result->addSuggestedAction($action);
            $result->setStatus('warning');
          }
          $result->setMessage($message);
          $results[] = $result;
        }
      }
    }
    else {
      $message = $this->t('The site has only one language: %language', ['%language' => reset($languages)->getName()]);
      $action = Link::createFromRoute($this->t('Add the desired languages.'), 'entity.configurable_language.collection');
      $result = new MultilingualAuditReportCheckResult();
      $result->setMessage($message)
        ->addSuggestedAction($action)
        ->setStatus('error');
      $results[] = $result;
    }
    return $results;
  }

}
