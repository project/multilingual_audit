<?php

namespace Drupal\multilingual_audit\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Component\Utility\Html;
use Drupal\Core\Link;
use Drupal\Core\Url;

class AuditReportController extends ControllerBase {

  public function overview() {
    $allResults = [];
    $header = [
      'icon' => [],
      'severity' => [
        'data' => $this->t('Severity'),
        'class' => [RESPONSIVE_PRIORITY_LOW],
      ],
      'description' => [
        'data' => $this->t('Description'),
      ],
      'actions' => [
        'data' => $this->t('Actions suggested'),
        'class' => [RESPONSIVE_PRIORITY_MEDIUM],
      ],
    ];

    $type = \Drupal::service('plugin.manager.multilingual_audit_report_check');
    $plugin_definitions = $type->getDefinitions();
    uasort($plugin_definitions, 'Drupal\Component\Utility\SortArray::sortByWeightElement');

    foreach ($plugin_definitions as $plugin_definition_id => $plugin_definition) {
      /** @var \Drupal\multilingual_audit\MultilingualAuditReportCheckInterface $plugin */
      $plugin = $type->createInstance($plugin_definition_id, []);
      if ($plugin->checkRequirements()) {
        $allResults[$plugin_definition_id] = $plugin->run();
      }
    }

    $rows = [];
    /** @var \Drupal\multilingual_audit\MultilingualAuditReportCheckResult[] $results */
    foreach ($allResults as $plugin_definition_id => $results) {
      $rows[] = [
        'class' => [Html::getClass('multilingual-audit-subcategory')],
        'data' => [
          [
            'data' => $plugin_definitions[$plugin_definition_id]['title']->get(),
            'colspan' => 4,
          ],
        ],
      ];
      $rows[] = [
        'class' => [Html::getClass('multilingual-audit-subcategory-description')],
        'data' => [
          [
            'data' => $plugin_definitions[$plugin_definition_id]['description']->get(),
            'colspan' => 4,
          ],
        ],
      ];

      foreach ($results as $result) {
        $actions = $result->getSuggestedActions();
        $dismissAction = new Link($this->t('Dismiss'), Url::fromUri('route:<none>#t'));
        // TODO: Hack, find a better way.
        $dismissAction->dismiss = TRUE;

        $actions[] = $dismissAction;
        $actionsComponent = NULL;
        if (!empty($actions)) {
          $actionsComponent = [
            '#theme' => 'links',
            '#attributes' => ['class' => ['suggested-actions-list']],
          ];
          foreach ($actions as $action) {
            $link = [
              '#type' => 'link',
              'title' => $action->getText(),
              'url' => $action->getUrl(),
            ];
            if (isset($action->dismiss) && $action->dismiss) {
              $link['attributes']['data-audit-dismiss'] = TRUE;
            }
            $actionsComponent['#links'][] = $link;
          }
        }

        $rows[] = [
          'data' => [
            'icon' => ['class' => ['icon']],
            'severity' => ['data' => ['#markup' => $result->getStatus()]],
            'description' => ['data' => ['#markup' => $result->getMessage()]],
            'actions' => ['data' => $actionsComponent],
          ],
          'class' => [Html::getClass('multilingual-audit-' . $result->getStatus())],
        ];
      }
    }

    $build['audit_table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#attributes' => ['id' => 'admin-multilingual-audit', 'class' => ['multilingual-audit']],
      '#empty' => $this->t('No audit information available.'),
      '#attached' => [
        'library' => ['multilingual_audit/multilingual_audit.report'],
      ],
    ];
    $build['audit_table_pager'] = ['#type' => 'pager'];

    return $build;
  }

}
