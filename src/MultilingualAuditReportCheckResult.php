<?php


namespace Drupal\multilingual_audit;


class MultilingualAuditReportCheckResult {

  /**
   * @var string
   */
  protected $status;

  /**
   * @var string
   */
  protected $message;

  /**
   * @var string[]|\Drupal\Core\Link[]
   */
  protected $suggestedActions;

  public function __construct() {
    $this->suggestedActions = [];
  }

  /**
   * @return int
   */
  public function getStatus() {
    return $this->status;
  }

  /**
   * @param int $status
   *
   * @return $this
   */
  public function setStatus(string $status) {
    $this->status = $status;
    return $this;
  }

  /**
   * @return array
   */
  public function getMessage() {
    return $this->message;
  }

  /**
   * @param string $message
   *
   * @return $this
   */
  public function setMessage($message) {
    $this->message = $message;
    return $this;
  }

  /**
   * @return string|\Drupal\Core\Link
   */
  public function getSuggestedActions() {
    return $this->suggestedActions;
  }

  /**
   * @param string|\Drupal\Core\Link $suggestedAction
   *
   * @return $this
   */
  public function addSuggestedAction($suggestedAction) {
    $this->suggestedActions[] = $suggestedAction;
    return $this;
  }

  /**
   * @param string[]|\Drupal\Core\Link[] $suggestedAction
   *
   * @return $this
   */
  public function addSuggestedActions($suggestedActions) {
    $this->suggestedActions = $suggestedActions;
    return $this;
  }

}
