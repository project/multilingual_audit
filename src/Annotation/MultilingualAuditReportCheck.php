<?php

namespace Drupal\multilingual_audit\Annotation;

use Drupal\Component\Annotation\AnnotationBase;

/**
 * Defines the MultilingualAuditReportCheck annotation.
 *
 * @Annotation
 */
class MultilingualAuditReportCheck extends AnnotationBase {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable title of the MultilingualAuditReportCheck type.
   *
   * @ingroup plugin_translatable
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public $title;

  /**
   * The human-readable description of the MultilingualAuditReportCheck type.
   *
   * @ingroup plugin_translatable
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public $description;

  /**
   * The plugin weight.
   *
   * @var int
   */
  public $weight;

  /**
   * @inheritDoc
   */
  public function get() {
    return [
      'id' => $this->id,
      'title' => $this->title,
      'description' => $this->description,
      'weight' => $this->weight,
      'class' => $this->class,
      'provider' => $this->provider,
    ];
  }

}
