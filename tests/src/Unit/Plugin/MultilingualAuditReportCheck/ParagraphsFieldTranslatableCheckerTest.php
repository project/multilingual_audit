<?php

namespace Drupal\Tests\multilingual_audit\Unit\Plugin\MultilingualAuditReportCheck;

use Drupal\content_translation\ContentTranslationManagerInterface;
use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Field\FieldConfigInterface;
use Drupal\Core\Link;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\lingotek\LingotekConfigurationServiceInterface;
use Drupal\multilingual_audit\Plugin\MultilingualAuditReportCheck\ParagraphsFieldTranslatableChecker;
use Drupal\Tests\UnitTestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Unit test for the paragraphs field translatable checker plugin.
 *
 * @coversDefaultClass \Drupal\multilingual_audit\Plugin\MultilingualAuditReportCheck\ParagraphsFieldTranslatableChecker
 * @group multilingual_audit
 * @preserveGlobalState disabled
 */
class ParagraphsFieldTranslatableCheckerTest extends UnitTestCase {

  /**
   * The class instance under test.
   *
   * @var \Drupal\multilingual_audit\Plugin\MultilingualAuditReportCheck\ParagraphsFieldTranslatableChecker
   */
  protected $checker;

  /**
   * The mocked module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $moduleHandler;

  /**
   * The mocked entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $entityTypeManager;

  /**
   * The mocked entity type bundle info.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $entityTypeBundleInfo;

  /**
   * The mocked entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $entityFieldManager;

  /**
   * The mocked content translation manager.
   *
   * @var \Drupal\content_translation\ContentTranslationManagerInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $contentTranslationManager;

  /**
   * The Lingotek configuration service.
   *
   * @var \Drupal\lingotek\LingotekConfigurationServiceInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $lingotekConfiguration;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->moduleHandler = $this->createMock(ModuleHandlerInterface::class);
    $this->entityTypeManager = $this->createMock(EntityTypeManagerInterface::class);
    $this->entityTypeBundleInfo = $this->createMock(EntityTypeBundleInfoInterface::class);
    $this->entityFieldManager = $this->createMock(EntityFieldManagerInterface::class);
    $this->contentTranslationManager = $this->createMock(ContentTranslationManagerInterface::class);
    $this->lingotekConfiguration = $this->createMock(LingotekConfigurationServiceInterface::class);

    $this->checker = new ParagraphsFieldTranslatableChecker([], 'paragraphs_field_translatable_checker', [], $this->moduleHandler, $this->entityTypeManager, $this->entityTypeBundleInfo, $this->entityFieldManager, $this->contentTranslationManager, $this->lingotekConfiguration);
    $this->checker->setStringTranslation($this->getStringTranslationStub());
  }

  /**
   * @covers ::__construct
   */
  public function testConstruct() {
    $checker = new ParagraphsFieldTranslatableChecker([], 'paragraphs_field_translatable_checker', [], $this->moduleHandler, $this->entityTypeManager, $this->entityTypeBundleInfo, $this->entityFieldManager, $this->contentTranslationManager);
    $this->assertNotNull($checker);
  }

  /**
   * @covers ::create
   */
  public function testCreate() {
    $container = $this->createMock(ContainerInterface::class);
    $container->expects($this->exactly(2))
      ->method('has')
      ->withConsecutive(['content_translation.manager'], ['lingotek.configuration'])
      ->willReturn(TRUE);
    $container->expects($this->exactly(6))
      ->method('get')
      ->withConsecutive(['content_translation.manager'], ['lingotek.configuration'], ['module_handler'], ['entity_type.manager'], ['entity_type.bundle.info'], ['entity_field.manager'])
      ->willReturnOnConsecutiveCalls($this->contentTranslationManager, $this->lingotekConfiguration, $this->moduleHandler, $this->entityTypeManager, $this->entityTypeBundleInfo, $this->entityFieldManager);
    $checker = ParagraphsFieldTranslatableChecker::create($container, [], 'paragraphs_field_translatable_checker', []);
    $this->assertNotNull($checker);
  }

  /**
   * @covers ::create
   */
  public function testCreateWithoutContentTranslation() {
    $container = $this->createMock(ContainerInterface::class);
    $container->expects($this->exactly(2))
      ->method('has')
      ->withConsecutive(['content_translation.manager'], ['lingotek.configuration'])
      ->willReturn(FALSE);
    $container->expects($this->exactly(4))
      ->method('get')
      ->withConsecutive(['module_handler'], ['entity_type.manager'], ['entity_type.bundle.info'], ['entity_field.manager'])
      ->willReturnOnConsecutiveCalls($this->moduleHandler, $this->entityTypeManager, $this->entityTypeBundleInfo, $this->entityFieldManager);
    $checker = ParagraphsFieldTranslatableChecker::create($container, [], 'paragraphs_field_translatable_checker', []);
    $this->assertNotNull($checker);
  }

  /**
   * @covers ::create
   */
  public function testCreateWithoutLingotek() {
    $container = $this->createMock(ContainerInterface::class);
    $container->expects($this->exactly(2))
      ->method('has')
      ->withConsecutive(['content_translation.manager'], ['lingotek.configuration'])
      ->willReturnOnConsecutiveCalls(TRUE, FALSE);
    $container->expects($this->exactly(5))
      ->method('get')
      ->withConsecutive(['content_translation.manager'], ['module_handler'], ['entity_type.manager'], ['entity_type.bundle.info'], ['entity_field.manager'])
      ->willReturnOnConsecutiveCalls($this->contentTranslationManager, $this->moduleHandler, $this->entityTypeManager, $this->entityTypeBundleInfo, $this->entityFieldManager);
    $checker = ParagraphsFieldTranslatableChecker::create($container, [], 'paragraphs_field_translatable_checker', []);
    $this->assertNotNull($checker);
  }

  /**
   * @covers ::checkRequirements
   */
  public function testCheckRequirements() {
    $this->moduleHandler->expects($this->exactly(4))
      ->method('moduleExists')
      ->withConsecutive(['content_translation'], ['paragraphs'], ['content_translation'], ['paragraphs'])
      ->willReturnOnConsecutiveCalls(TRUE, TRUE, TRUE, FALSE);
    $this->assertTrue($this->checker->checkRequirements());
    $this->assertFalse($this->checker->checkRequirements());
  }

  /**
   * @covers ::run
   */
  public function testRunWithParagraphFieldDefinitionsWithoutLanguage() {
    $entityType = $this->createMock(ContentEntityTypeInterface::class);
    $entityType->expects($this->once())
      ->method('hasKey')
      ->with('langcode')
      ->willReturn(FALSE);
    $this->entityTypeManager->expects($this->once())
      ->method('getDefinitions')
      ->willReturn(['entity_type' => $entityType]);

    $results = $this->checker->run();
    $this->assertEmpty($results);
  }

  /**
   * @covers ::run
   */
  public function testRunWithSomeParagraphFields() {
    $this->moduleHandler->expects($this->exactly(3))
      ->method('moduleExists')
      ->withConsecutive(['paragraphs_asymmetric_translation_widgets'], ['lingotek'], ['lingotek'])
      ->willReturn(FALSE);
    $entityType = $this->createMock(ContentEntityTypeInterface::class);
    $entityType->expects($this->any())
      ->method('hasKey')
      ->with('langcode')
      ->willReturn(TRUE);
    $entityType->expects($this->any())
      ->method('id')
      ->willReturn('entity_id');
    $entityType->expects($this->any())
      ->method('getBundleEntityType')
      ->willReturn('entity_id');
    $entityType->expects($this->any())
      ->method('getLabel')
      ->willReturn('Entity');

    $this->entityTypeManager->expects($this->once())
      ->method('getDefinitions')
      ->willReturn(['entity_id' => $entityType, 'entity_non_translatable' => $entityType]);
    $this->contentTranslationManager->expects($this->exactly(2))
      ->method('isSupported')
      ->withConsecutive(['entity_id'], ['entity_non_translatable'])
      ->willReturnOnConsecutiveCalls(TRUE, FALSE);
    $this->contentTranslationManager->expects($this->exactly(2))
      ->method('isEnabled')
      ->withConsecutive(['entity_id', 'entity_type_id'], ['entity_id', 'bundle_non_translatable'])
      ->willReturnOnConsecutiveCalls(TRUE, FALSE);
    $this->entityTypeBundleInfo->expects($this->once())
      ->method('getBundleInfo')
      ->with('entity_id')
      ->willReturn([
            'entity_type_id' => [
              'label' => 'Entity Type',
            ],
            'bundle_non_translatable' => [
              'label' => 'Entity Type Non Translatable',
            ],
        ]);
    $fieldTranslatableERRDefinition = $this->createMock(FieldConfigInterface::class);
    $fieldTranslatableERRDefinition->expects($this->any())
      ->method('id')
      ->willReturn('other_field');
    $fieldTranslatableERRDefinition->expects($this->any())
      ->method('getType')
      ->willReturn('other_type_field');
    $fieldTranslatableERRDefinition->expects($this->any())
      ->method('isTranslatable')
      ->willReturn(FALSE);
    $fieldTranslatableERRDefinition->expects($this->any())
      ->method('getName')
      ->willReturn('Other Type Field');

    $fieldTranslatableERRDefinition = $this->createMock(FieldConfigInterface::class);
    $fieldTranslatableERRDefinition->expects($this->any())
      ->method('id')
      ->willReturn('err_translatable');
    $fieldTranslatableERRDefinition->expects($this->any())
      ->method('getType')
      ->willReturn('entity_reference_revisions');
    $fieldTranslatableERRDefinition->expects($this->any())
      ->method('isTranslatable')
      ->willReturn(TRUE);
    $fieldTranslatableERRDefinition->expects($this->any())
      ->method('getName')
      ->willReturn('ERR Translatable');

    $fieldNonTranslatableERRDefinition = $this->createMock(FieldConfigInterface::class);
    $fieldNonTranslatableERRDefinition->expects($this->any())
      ->method('id')
      ->willReturn('err_non_translatable');
    $fieldNonTranslatableERRDefinition->expects($this->any())
      ->method('getType')
      ->willReturn('entity_reference_revisions');
    $fieldNonTranslatableERRDefinition->expects($this->any())
      ->method('isTranslatable')
      ->willReturn(FALSE);
    $fieldNonTranslatableERRDefinition->expects($this->any())
      ->method('getName')
      ->willReturn('ERR Non Translatable');

    $this->entityFieldManager->expects($this->once())
      ->method('getFieldDefinitions')
      ->with('entity_id', 'entity_type_id')
      ->willReturn([
        'err_translatable' => $fieldTranslatableERRDefinition,
        'err_non_translatable' => $fieldNonTranslatableERRDefinition,
      ]);

    $results = $this->checker->run();
    $this->assertCount(2, $results);

    $this->assertEquals('warning', $results[0]->getStatus());
    $message = new TranslatableMarkup('The entity %entity bundle %bundle entity_reference_revision field %field is enabled for content translation. The paragraphs module is not supporting that scenario.', [
      '%entity' => 'Entity',
      '%bundle' => 'Entity Type',
      '%field' => 'ERR Translatable',
    ], [], $this->getStringTranslationStub());
    $this->assertEquals($message, $results[0]->getMessage());
    $actionMessage = new TranslatableMarkup('Enable %module module.', ['%module' => 'paragraphs_asymmetric_translation_widgets'], [], $this->getStringTranslationStub());
    $action = Link::createFromRoute($actionMessage, 'system.modules_list');
    $this->assertEquals([$action], $results[0]->getSuggestedActions());

    $this->assertEquals('ok', $results[1]->getStatus());
    $message = new TranslatableMarkup('The entity %entity bundle %bundle entity_reference_revision field %field is not enabled for content translation.', [
      '%entity' => 'Entity',
      '%bundle' => 'Entity Type',
      '%field' => 'ERR Non Translatable',
    ], [], $this->getStringTranslationStub());
    $this->assertEquals($message, $results[1]->getMessage());
    $actionMessage = new TranslatableMarkup('Enable it.', [
      '%entity' => 'Entity',
      '%bundle' => 'Entity Type',
      '%field' => 'ERR Non Translatable',
    ], [], $this->getStringTranslationStub());
    $action = Link::createFromRoute($actionMessage, 'entity.field_config.entity_id_field_edit_form', [
      'entity_id' => 'entity_type_id',
      'field_config' => 'err_non_translatable',
    ]);
    $this->assertEquals([$action], $results[1]->getSuggestedActions());
  }

  /**
   * @covers ::run
   */
  public function testRunWithSomeParagraphFieldsAndLingotek() {
    $this->moduleHandler->expects($this->exactly(3))
      ->method('moduleExists')
      ->withConsecutive(['paragraphs_asymmetric_translation_widgets'], ['lingotek'], ['lingotek'])
      ->willReturnOnConsecutiveCalls(FALSE, TRUE, TRUE);
    $this->lingotekConfiguration->expects($this->exactly(2))
      ->method('isEnabled')
      ->withConsecutive(['entity_id', 'entity_type_id'], ['entity_id', 'entity_type_id'])
      ->willReturnOnConsecutiveCalls(TRUE, TRUE);
    $this->lingotekConfiguration->expects($this->exactly(2))
      ->method('isFieldLingotekEnabled')
      ->withConsecutive(['entity_id', 'entity_type_id', 'err_translatable'], ['entity_id', 'entity_type_id', 'err_non_translatable'])
      ->willReturnOnConsecutiveCalls(FALSE, TRUE);

    $entityType = $this->createMock(ContentEntityTypeInterface::class);
    $entityType->expects($this->any())
      ->method('hasKey')
      ->with('langcode')
      ->willReturn(TRUE);
    $entityType->expects($this->any())
      ->method('id')
      ->willReturn('entity_id');
    $entityType->expects($this->any())
      ->method('getBundleEntityType')
      ->willReturn('entity_id');
    $entityType->expects($this->any())
      ->method('getLabel')
      ->willReturn('Entity');

    $this->entityTypeManager->expects($this->once())
      ->method('getDefinitions')
      ->willReturn(['entity_id' => $entityType, 'entity_non_translatable' => $entityType]);
    $this->contentTranslationManager->expects($this->exactly(2))
      ->method('isSupported')
      ->withConsecutive(['entity_id'], ['entity_non_translatable'])
      ->willReturnOnConsecutiveCalls(TRUE, FALSE);
    $this->contentTranslationManager->expects($this->exactly(2))
      ->method('isEnabled')
      ->withConsecutive(['entity_id', 'entity_type_id'], ['entity_id', 'bundle_non_translatable'])
      ->willReturnOnConsecutiveCalls(TRUE, FALSE);
    $this->entityTypeBundleInfo->expects($this->once())
      ->method('getBundleInfo')
      ->with('entity_id')
      ->willReturn([
        'entity_type_id' => [
          'label' => 'Entity Type',
        ],
        'bundle_non_translatable' => [
          'label' => 'Entity Type Non Translatable',
        ],
      ]);
    $fieldTranslatableERRDefinition = $this->createMock(FieldConfigInterface::class);
    $fieldTranslatableERRDefinition->expects($this->any())
      ->method('id')
      ->willReturn('other_field');
    $fieldTranslatableERRDefinition->expects($this->any())
      ->method('getType')
      ->willReturn('other_type_field');
    $fieldTranslatableERRDefinition->expects($this->any())
      ->method('isTranslatable')
      ->willReturn(FALSE);
    $fieldTranslatableERRDefinition->expects($this->any())
      ->method('getName')
      ->willReturn('Other Type Field');

    $fieldTranslatableERRDefinition = $this->createMock(FieldConfigInterface::class);
    $fieldTranslatableERRDefinition->expects($this->any())
      ->method('id')
      ->willReturn('err_translatable');
    $fieldTranslatableERRDefinition->expects($this->any())
      ->method('getType')
      ->willReturn('entity_reference_revisions');
    $fieldTranslatableERRDefinition->expects($this->any())
      ->method('isTranslatable')
      ->willReturn(TRUE);
    $fieldTranslatableERRDefinition->expects($this->any())
      ->method('getName')
      ->willReturn('ERR Translatable');

    $fieldNonTranslatableERRDefinition = $this->createMock(FieldConfigInterface::class);
    $fieldNonTranslatableERRDefinition->expects($this->any())
      ->method('id')
      ->willReturn('err_non_translatable');
    $fieldNonTranslatableERRDefinition->expects($this->any())
      ->method('getType')
      ->willReturn('entity_reference_revisions');
    $fieldNonTranslatableERRDefinition->expects($this->any())
      ->method('isTranslatable')
      ->willReturn(FALSE);
    $fieldNonTranslatableERRDefinition->expects($this->any())
      ->method('getName')
      ->willReturn('ERR Non Translatable');

    $this->entityFieldManager->expects($this->once())
      ->method('getFieldDefinitions')
      ->with('entity_id', 'entity_type_id')
      ->willReturn([
        'err_translatable' => $fieldTranslatableERRDefinition,
        'err_non_translatable' => $fieldNonTranslatableERRDefinition,
      ]);

    $results = $this->checker->run();
    $this->assertCount(3, $results);

    $this->assertEquals('warning', $results[0]->getStatus());
    $message = new TranslatableMarkup('The entity %entity bundle %bundle entity_reference_revision field %field is enabled for content translation. The paragraphs module is not supporting that scenario.', [
      '%entity' => 'Entity',
      '%bundle' => 'Entity Type',
      '%field' => 'ERR Translatable',
    ], [], $this->getStringTranslationStub());
    $this->assertEquals($message, $results[0]->getMessage());
    $actionMessage = new TranslatableMarkup('Enable %module module.', ['%module' => 'paragraphs_asymmetric_translation_widgets'], [], $this->getStringTranslationStub());
    $action = Link::createFromRoute($actionMessage, 'system.modules_list');
    $this->assertEquals([$action], $results[0]->getSuggestedActions());

    $this->assertEquals('warning', $results[1]->getStatus());
    $message = new TranslatableMarkup('The entity %entity bundle %bundle entity_reference_revision field %field is not enabled for Lingotek translation. You probably want to embed your paragraphs in the parent document.', [
      '%entity' => 'Entity',
      '%bundle' => 'Entity Type',
      '%field' => 'ERR Translatable',
    ], [], $this->getStringTranslationStub());
    $this->assertEquals($message, $results[1]->getMessage());
    $actionMessage = new TranslatableMarkup('Change Lingotek translatability of this field.', [], [], $this->getStringTranslationStub());
    $action = Link::createFromRoute($actionMessage, 'lingotek.settings.content_form', [
      'entity_type' => 'entity_id',
      'bundle' => 'entity_type_id',
    ]);
    $this->assertEquals([$action], $results[1]->getSuggestedActions());

    $this->assertEquals('ok', $results[2]->getStatus());
    $message = new TranslatableMarkup('The entity %entity bundle %bundle entity_reference_revision field %field is not enabled for content translation.', [
      '%entity' => 'Entity',
      '%bundle' => 'Entity Type',
      '%field' => 'ERR Non Translatable',
    ], [], $this->getStringTranslationStub());
    $this->assertEquals($message, $results[2]->getMessage());
    $actionMessage = new TranslatableMarkup('Enable it.', [
      '%entity' => 'Entity',
      '%bundle' => 'Entity Type',
      '%field' => 'ERR Non Translatable',
    ], [], $this->getStringTranslationStub());
    $action = Link::createFromRoute($actionMessage, 'entity.field_config.entity_id_field_edit_form', [
      'entity_id' => 'entity_type_id',
      'field_config' => 'err_non_translatable',
    ]);
    $this->assertEquals([$action], $results[2]->getSuggestedActions());
  }

  /**
   * @covers ::run
   */
  public function testRunWithSomeParagraphFieldsAndAssymetricParagraphs() {
    $this->moduleHandler->expects($this->exactly(3))
      ->method('moduleExists')
      ->withConsecutive(['paragraphs_asymmetric_translation_widgets'], ['lingotek'], ['lingotek'])
      ->willReturnOnConsecutiveCalls(TRUE, FALSE, FALSE);
    $entityType = $this->createMock(ContentEntityTypeInterface::class);
    $entityType->expects($this->any())
      ->method('hasKey')
      ->with('langcode')
      ->willReturn(TRUE);
    $entityType->expects($this->any())
      ->method('id')
      ->willReturn('entity_id');
    $entityType->expects($this->any())
      ->method('getBundleEntityType')
      ->willReturn('entity_id');
    $entityType->expects($this->any())
      ->method('getLabel')
      ->willReturn('Entity');

    $this->entityTypeManager->expects($this->once())
      ->method('getDefinitions')
      ->willReturn(['entity_id' => $entityType, 'entity_non_translatable' => $entityType]);
    $this->contentTranslationManager->expects($this->exactly(2))
      ->method('isSupported')
      ->withConsecutive(['entity_id'], ['entity_non_translatable'])
      ->willReturnOnConsecutiveCalls(TRUE, FALSE);
    $this->contentTranslationManager->expects($this->exactly(2))
      ->method('isEnabled')
      ->withConsecutive(['entity_id', 'entity_type_id'], ['entity_id', 'bundle_non_translatable'])
      ->willReturnOnConsecutiveCalls(TRUE, FALSE);
    $this->entityTypeBundleInfo->expects($this->once())
      ->method('getBundleInfo')
      ->with('entity_id')
      ->willReturn([
        'entity_type_id' => [
          'label' => 'Entity Type',
        ],
        'bundle_non_translatable' => [
          'label' => 'Entity Type Non Translatable',
        ],
      ]);
    $fieldTranslatableERRDefinition = $this->createMock(FieldConfigInterface::class);
    $fieldTranslatableERRDefinition->expects($this->any())
      ->method('id')
      ->willReturn('other_field');
    $fieldTranslatableERRDefinition->expects($this->any())
      ->method('getType')
      ->willReturn('other_type_field');
    $fieldTranslatableERRDefinition->expects($this->any())
      ->method('isTranslatable')
      ->willReturn(FALSE);
    $fieldTranslatableERRDefinition->expects($this->any())
      ->method('getName')
      ->willReturn('Other Type Field');

    $fieldTranslatableERRDefinition = $this->createMock(FieldConfigInterface::class);
    $fieldTranslatableERRDefinition->expects($this->any())
      ->method('id')
      ->willReturn('err_translatable');
    $fieldTranslatableERRDefinition->expects($this->any())
      ->method('getType')
      ->willReturn('entity_reference_revisions');
    $fieldTranslatableERRDefinition->expects($this->any())
      ->method('isTranslatable')
      ->willReturn(TRUE);
    $fieldTranslatableERRDefinition->expects($this->any())
      ->method('getName')
      ->willReturn('ERR Translatable');

    $fieldNonTranslatableERRDefinition = $this->createMock(FieldConfigInterface::class);
    $fieldNonTranslatableERRDefinition->expects($this->any())
      ->method('id')
      ->willReturn('err_non_translatable');
    $fieldNonTranslatableERRDefinition->expects($this->any())
      ->method('getType')
      ->willReturn('entity_reference_revisions');
    $fieldNonTranslatableERRDefinition->expects($this->any())
      ->method('isTranslatable')
      ->willReturn(FALSE);
    $fieldNonTranslatableERRDefinition->expects($this->any())
      ->method('getName')
      ->willReturn('ERR Non Translatable');

    $this->entityFieldManager->expects($this->once())
      ->method('getFieldDefinitions')
      ->with('entity_id', 'entity_type_id')
      ->willReturn([
        'err_translatable' => $fieldTranslatableERRDefinition,
        'err_non_translatable' => $fieldNonTranslatableERRDefinition,
      ]);

    $results = $this->checker->run();
    $this->assertCount(2, $results);

    $this->assertEquals('ok', $results[0]->getStatus());
    $message = new TranslatableMarkup('The entity %entity bundle %bundle entity_reference_revision field %field is enabled for content translation. Ensure you use a %module module widget in your form displays', [
      '%entity' => 'Entity',
      '%bundle' => 'Entity Type',
      '%field' => 'ERR Translatable',
      '%module' => 'paragraphs_asymmetric_translation_widgets',
    ], [], $this->getStringTranslationStub());
    $this->assertEquals($message, $results[0]->getMessage());
    $this->assertEmpty($results[0]->getSuggestedActions());

    $this->assertEquals('warning', $results[1]->getStatus());
    $message = new TranslatableMarkup('The entity %entity bundle %bundle entity_reference_revision field %field is not enabled for content translation, but %module module is present. Review this is the desired setting.', [
      '%entity' => 'Entity',
      '%bundle' => 'Entity Type',
      '%field' => 'ERR Non Translatable',
      '%module' => 'paragraphs_asymmetric_translation_widgets',
    ], [], $this->getStringTranslationStub());
    $this->assertEquals($message, $results[1]->getMessage());
    $actionMessage = new TranslatableMarkup('Review the translatability of this field.', [
      '%entity' => 'Entity',
      '%bundle' => 'Entity Type',
      '%field' => 'ERR Non Translatable',
    ], [], $this->getStringTranslationStub());
    $action = Link::createFromRoute($actionMessage, 'entity.field_config.entity_id_field_edit_form', [
      'entity_id' => 'entity_type_id',
      'field_config' => 'err_non_translatable',
    ]);
    $this->assertEquals([$action], $results[1]->getSuggestedActions());
  }


  /**
   * @covers ::run
   */
  public function testRunWithSomeParagraphFieldsAndAssymetricParagraphsAndLingotek() {
    $this->moduleHandler->expects($this->exactly(3))
      ->method('moduleExists')
      ->withConsecutive(['paragraphs_asymmetric_translation_widgets'], ['lingotek'], ['lingotek'])
      ->willReturn(TRUE);
    $this->lingotekConfiguration->expects($this->exactly(2))
      ->method('isEnabled')
      ->withConsecutive(['entity_id', 'entity_type_id'], ['entity_id', 'entity_type_id'])
      ->willReturnOnConsecutiveCalls(TRUE, TRUE);
    $this->lingotekConfiguration->expects($this->exactly(2))
      ->method('isFieldLingotekEnabled')
      ->withConsecutive(['entity_id', 'entity_type_id', 'err_translatable'], ['entity_id', 'entity_type_id', 'err_non_translatable'])
      ->willReturnOnConsecutiveCalls(FALSE, TRUE);

    $entityType = $this->createMock(ContentEntityTypeInterface::class);
    $entityType->expects($this->any())
      ->method('hasKey')
      ->with('langcode')
      ->willReturn(TRUE);
    $entityType->expects($this->any())
      ->method('id')
      ->willReturn('entity_id');
    $entityType->expects($this->any())
      ->method('getBundleEntityType')
      ->willReturn('entity_id');
    $entityType->expects($this->any())
      ->method('getLabel')
      ->willReturn('Entity');

    $this->entityTypeManager->expects($this->once())
      ->method('getDefinitions')
      ->willReturn(['entity_id' => $entityType, 'entity_non_translatable' => $entityType]);
    $this->contentTranslationManager->expects($this->exactly(2))
      ->method('isSupported')
      ->withConsecutive(['entity_id'], ['entity_non_translatable'])
      ->willReturnOnConsecutiveCalls(TRUE, FALSE);
    $this->contentTranslationManager->expects($this->exactly(2))
      ->method('isEnabled')
      ->withConsecutive(['entity_id', 'entity_type_id'], ['entity_id', 'bundle_non_translatable'])
      ->willReturnOnConsecutiveCalls(TRUE, FALSE);
    $this->entityTypeBundleInfo->expects($this->once())
      ->method('getBundleInfo')
      ->with('entity_id')
      ->willReturn([
        'entity_type_id' => [
          'label' => 'Entity Type',
        ],
        'bundle_non_translatable' => [
          'label' => 'Entity Type Non Translatable',
        ],
      ]);
    $fieldTranslatableERRDefinition = $this->createMock(FieldConfigInterface::class);
    $fieldTranslatableERRDefinition->expects($this->any())
      ->method('id')
      ->willReturn('other_field');
    $fieldTranslatableERRDefinition->expects($this->any())
      ->method('getType')
      ->willReturn('other_type_field');
    $fieldTranslatableERRDefinition->expects($this->any())
      ->method('isTranslatable')
      ->willReturn(FALSE);
    $fieldTranslatableERRDefinition->expects($this->any())
      ->method('getName')
      ->willReturn('Other Type Field');

    $fieldTranslatableERRDefinition = $this->createMock(FieldConfigInterface::class);
    $fieldTranslatableERRDefinition->expects($this->any())
      ->method('id')
      ->willReturn('err_translatable');
    $fieldTranslatableERRDefinition->expects($this->any())
      ->method('getType')
      ->willReturn('entity_reference_revisions');
    $fieldTranslatableERRDefinition->expects($this->any())
      ->method('isTranslatable')
      ->willReturn(TRUE);
    $fieldTranslatableERRDefinition->expects($this->any())
      ->method('getName')
      ->willReturn('ERR Translatable');

    $fieldNonTranslatableERRDefinition = $this->createMock(FieldConfigInterface::class);
    $fieldNonTranslatableERRDefinition->expects($this->any())
      ->method('id')
      ->willReturn('err_non_translatable');
    $fieldNonTranslatableERRDefinition->expects($this->any())
      ->method('getType')
      ->willReturn('entity_reference_revisions');
    $fieldNonTranslatableERRDefinition->expects($this->any())
      ->method('isTranslatable')
      ->willReturn(FALSE);
    $fieldNonTranslatableERRDefinition->expects($this->any())
      ->method('getName')
      ->willReturn('ERR Non Translatable');

    $this->entityFieldManager->expects($this->once())
      ->method('getFieldDefinitions')
      ->with('entity_id', 'entity_type_id')
      ->willReturn([
        'err_translatable' => $fieldTranslatableERRDefinition,
        'err_non_translatable' => $fieldNonTranslatableERRDefinition,
      ]);

    $results = $this->checker->run();
    $this->assertCount(3, $results);

    $this->assertEquals('ok', $results[0]->getStatus());
    $message = new TranslatableMarkup('The entity %entity bundle %bundle entity_reference_revision field %field is enabled for content translation. Ensure you use a %module module widget in your form displays', [
      '%entity' => 'Entity',
      '%bundle' => 'Entity Type',
      '%field' => 'ERR Translatable',
      '%module' => 'paragraphs_asymmetric_translation_widgets',
    ], [], $this->getStringTranslationStub());
    $this->assertEquals($message, $results[0]->getMessage());
    $this->assertEmpty($results[0]->getSuggestedActions());

    $this->assertEquals('warning', $results[1]->getStatus());
    $message = new TranslatableMarkup('The entity %entity bundle %bundle entity_reference_revision field %field is not enabled for Lingotek translation. You probably want to embed your paragraphs in the parent document.', [
      '%entity' => 'Entity',
      '%bundle' => 'Entity Type',
      '%field' => 'ERR Translatable',
    ], [], $this->getStringTranslationStub());
    $this->assertEquals($message, $results[1]->getMessage());
    $actionMessage = new TranslatableMarkup('Change Lingotek translatability of this field.', [], [], $this->getStringTranslationStub());
    $action = Link::createFromRoute($actionMessage, 'lingotek.settings.content_form', [
      'entity_type' => 'entity_id',
      'bundle' => 'entity_type_id',
    ]);
    $this->assertEquals([$action], $results[1]->getSuggestedActions());

    $this->assertEquals('warning', $results[2]->getStatus());
    $message = new TranslatableMarkup('The entity %entity bundle %bundle entity_reference_revision field %field is not enabled for content translation, but %module module is present. Review this is the desired setting.', [
      '%entity' => 'Entity',
      '%bundle' => 'Entity Type',
      '%field' => 'ERR Non Translatable',
      '%module' => 'paragraphs_asymmetric_translation_widgets',
    ], [], $this->getStringTranslationStub());
    $this->assertEquals($message, $results[2]->getMessage());
    $actionMessage = new TranslatableMarkup('Review the translatability of this field.', [
      '%entity' => 'Entity',
      '%bundle' => 'Entity Type',
      '%field' => 'ERR Non Translatable',
    ], [], $this->getStringTranslationStub());
    $action = Link::createFromRoute($actionMessage, 'entity.field_config.entity_id_field_edit_form', [
      'entity_id' => 'entity_type_id',
      'field_config' => 'err_non_translatable',
    ]);
    $this->assertEquals([$action], $results[2]->getSuggestedActions());
  }

}
