<?php

namespace Drupal\Tests\multilingual_audit\Unit\Plugin\MultilingualAuditReportCheck;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Link;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\multilingual_audit\Plugin\MultilingualAuditReportCheck\MultilingualModulesChecker;
use Drupal\Tests\UnitTestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Unit test for the multilingual modules checker plugin.
 *
 * @coversDefaultClass \Drupal\multilingual_audit\Plugin\MultilingualAuditReportCheck\MultilingualModulesChecker
 * @group multilingual_audit
 * @preserveGlobalState disabled
 */
class MultilingualModulesCheckerTest extends UnitTestCase {

  /**
   * The class instance under test.
   *
   * @var \Drupal\multilingual_audit\Plugin\MultilingualAuditReportCheck\MultilingualModulesChecker
   */
  protected $checker;

  /**
   * The mocked module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $moduleHandler;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->moduleHandler = $this->createMock(ModuleHandlerInterface::class);

    $this->checker = new MultilingualModulesChecker([], 'multilingual_modules_checker', [], $this->moduleHandler);
    $this->checker->setStringTranslation($this->getStringTranslationStub());
  }

  /**
   * @covers ::__construct
   */
  public function testConstruct() {
    $checker = new MultilingualModulesChecker([], 'multilingual_modules_checker', [], $this->moduleHandler);
    $this->assertNotNull($checker);
  }

  /**
   * @covers ::create
   */
  public function testCreate() {
    $container = $this->createMock(ContainerInterface::class);
    $container->expects($this->exactly(1))
      ->method('get')
      ->with('module_handler')
      ->willReturn($this->moduleHandler);
    $checker = MultilingualModulesChecker::create($container, [], 'multilingual_modules_checker', []);
    $this->assertNotNull($checker);
  }

  /**
   * @covers ::checkRequirements
   */
  public function testCheckRequirements() {
    $this->assertTrue($this->checker->checkRequirements());
  }

  /**
   * @covers ::run
   */
  public function testRunMissingRequiredModules() {
    $this->moduleHandler->expects($this->exactly(3))
      ->method('moduleExists')
      ->withConsecutive(['language'], ['content_translation'], ['lingotek'])
      ->willReturnOnConsecutiveCalls(FALSE, FALSE, FALSE);

    $results = $this->checker->run();
    $this->assertCount(3, $results);

    $this->assertEquals('error', $results[0]->getStatus());
    $message = new TranslatableMarkup('Module %module is not enabled.', ['%module' => 'language'], [], $this->getStringTranslationStub());
    $this->assertEquals($message, $results[0]->getMessage());
    $actionMessage = new TranslatableMarkup('Enable %module module.', ['%module' => 'language'], [], $this->getStringTranslationStub());
    $action = Link::createFromRoute($actionMessage, 'system.modules_list');
    $this->assertEquals([$action], $results[0]->getSuggestedActions());

    $this->assertEquals('error', $results[1]->getStatus());
    $message = new TranslatableMarkup('Module %module is not enabled.', ['%module' => 'content_translation'], [], $this->getStringTranslationStub());
    $this->assertEquals($message, $results[1]->getMessage());
    $actionMessage = new TranslatableMarkup('Enable %module module.', ['%module' => 'content_translation'], [], $this->getStringTranslationStub());
    $action = Link::createFromRoute($actionMessage, 'system.modules_list');
    $this->assertEquals([$action], $results[1]->getSuggestedActions());

    $this->assertEquals('warning', $results[2]->getStatus());
    $message = new TranslatableMarkup('Module %module is not enabled.', ['%module' => 'lingotek'], [], $this->getStringTranslationStub());
    $this->assertEquals($message, $results[2]->getMessage());
    $actionMessage = new TranslatableMarkup('Enable %module module.', ['%module' => 'lingotek'], [], $this->getStringTranslationStub());
    $action = Link::createFromRoute($actionMessage, 'system.modules_list');
    $this->assertEquals([$action], $results[2]->getSuggestedActions());
  }

  /**
   * @covers ::run
   */
  public function testRunMissingSuggestedModules() {
    $this->moduleHandler->expects($this->exactly(3))
      ->method('moduleExists')
      ->withConsecutive(['language'], ['content_translation'], ['lingotek'])
      ->willReturnOnConsecutiveCalls(TRUE, TRUE, FALSE);

    $results = $this->checker->run();
    $this->assertCount(3, $results);

    $this->assertEquals('ok', $results[0]->getStatus());
    $message = new TranslatableMarkup('Module %module is enabled.', ['%module' => 'language'], [], $this->getStringTranslationStub());
    $this->assertEquals($message, $results[0]->getMessage());
    $this->assertEmpty($results[0]->getSuggestedActions());

    $this->assertEquals('ok', $results[1]->getStatus());
    $message = new TranslatableMarkup('Module %module is enabled.', ['%module' => 'content_translation'], [], $this->getStringTranslationStub());
    $this->assertEquals($message, $results[1]->getMessage());
    $this->assertEmpty($results[1]->getSuggestedActions());

    $this->assertEquals('warning', $results[2]->getStatus());
    $message = new TranslatableMarkup('Module %module is not enabled.', ['%module' => 'lingotek'], [], $this->getStringTranslationStub());
    $this->assertEquals($message, $results[2]->getMessage());
    $actionMessage = new TranslatableMarkup('Enable %module module.', ['%module' => 'lingotek'], [], $this->getStringTranslationStub());
    $action = Link::createFromRoute($actionMessage, 'system.modules_list');
    $this->assertEquals([$action], $results[2]->getSuggestedActions());
  }

  /**
   * @covers ::run
   */
  public function testRunWithRequiredAndSuggestedModules() {
    $results = $this->checker->run();
    $this->assertCount(3, $results);
    $this->moduleHandler->expects($this->exactly(3))
      ->method('moduleExists')
      ->withConsecutive(['language'], ['content_translation'], ['lingotek'])
      ->willReturnOnConsecutiveCalls(TRUE, TRUE, TRUE);

    $results = $this->checker->run();
    $this->assertCount(3, $results);

    $this->assertEquals('ok', $results[0]->getStatus());
    $message = new TranslatableMarkup('Module %module is enabled.', ['%module' => 'language'], [], $this->getStringTranslationStub());
    $this->assertEquals($message, $results[0]->getMessage());
    $this->assertEmpty($results[0]->getSuggestedActions());

    $this->assertEquals('ok', $results[1]->getStatus());
    $message = new TranslatableMarkup('Module %module is enabled.', ['%module' => 'content_translation'], [], $this->getStringTranslationStub());
    $this->assertEquals($message, $results[1]->getMessage());
    $this->assertEmpty($results[1]->getSuggestedActions());

    $this->assertEquals('ok', $results[2]->getStatus());
    $message = new TranslatableMarkup('Module %module is enabled.', ['%module' => 'lingotek'], [], $this->getStringTranslationStub());
    $this->assertEquals($message, $results[2]->getMessage());
    $this->assertEmpty($results[2]->getSuggestedActions());
  }

}
