<?php

namespace Drupal\Tests\multilingual_audit\Unit\Plugin\MultilingualAuditReportCheck;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Link;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\language\ConfigurableLanguageInterface;
use Drupal\lingotek\LanguageLocaleMapperInterface;
use Drupal\multilingual_audit\Plugin\MultilingualAuditReportCheck\LanguagesChecker;
use Drupal\Tests\UnitTestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Unit test for the languages checker plugin.
 *
 * @coversDefaultClass \Drupal\multilingual_audit\Plugin\MultilingualAuditReportCheck\LanguagesChecker
 * @group multilingual_audit
 * @preserveGlobalState disabled
 */
class LanguagesCheckerTest extends UnitTestCase {

  /**
   * The class instance under test.
   *
   * @var \Drupal\multilingual_audit\Plugin\MultilingualAuditReportCheck\LanguagesChecker
   */
  protected $checker;

  /**
   * The mocked module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $moduleHandler;

  /**
   * The mocked language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $languageManager;

  /**
   * The mocked entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $entityTypeManager;

  /**
   * The mocked language-locale mapper.
   *
   * @var \Drupal\lingotek\LanguageLocaleMapperInterface|\PHPUnit\Framework\MockObject\MockObject|null
   */
  protected $languageLocaleMapper;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->moduleHandler = $this->createMock(ModuleHandlerInterface::class);
    $this->languageManager = $this->createMock(LanguageManagerInterface::class);
    $this->entityTypeManager = $this->createMock(EntityTypeManagerInterface::class);
    $this->languageLocaleMapper = $this->createMock(LanguageLocaleMapperInterface::class);

    $this->checker = new LanguagesChecker([], 'languages_checker', [], $this->moduleHandler, $this->languageManager, $this->entityTypeManager, $this->languageLocaleMapper);
    $this->checker->setStringTranslation($this->getStringTranslationStub());
  }

  /**
   * @covers ::__construct
   */
  public function testConstruct() {
    $checker = new LanguagesChecker([], 'languages_checker', [], $this->moduleHandler, $this->languageManager, $this->entityTypeManager, $this->languageLocaleMapper);
    $this->assertNotNull($checker);
  }

  /**
   * @covers ::__construct
   */
  public function testConstructorWithoutLingotek() {
    $checker = new LanguagesChecker([], 'languages_checker', [], $this->moduleHandler, $this->languageManager, $this->entityTypeManager, NULL);
    $this->assertNotNull($checker);
  }

  /**
   * @covers ::create
   */
  public function testCreate() {
    $container = $this->createMock(ContainerInterface::class);
    $container->expects($this->once())
      ->method('has')
      ->with('lingotek.language_locale_mapper')
      ->willReturn(TRUE);

    $container->expects($this->exactly(4))
      ->method('get')
      ->withConsecutive(['lingotek.language_locale_mapper'], ['module_handler'], ['language_manager'], ['entity_type.manager'])
      ->willReturnOnConsecutiveCalls($this->languageLocaleMapper, $this->moduleHandler, $this->languageManager, $this->entityTypeManager);
    $checker = LanguagesChecker::create($container, [], 'languages_checker', []);
    $this->assertNotNull($checker);
  }

  /**
   * @covers ::create
   */
  public function testCreateWithoutLingotek() {
    $container = $this->createMock(ContainerInterface::class);
    $container->expects($this->once())
      ->method('has')
      ->with('lingotek.language_locale_mapper')
      ->willReturn(FALSE);
    $container->expects($this->exactly(3))
      ->method('get')
      ->withConsecutive(['module_handler'], ['language_manager'], ['entity_type.manager'])
      ->willReturnOnConsecutiveCalls($this->moduleHandler, $this->languageManager, $this->entityTypeManager);
    $checker = LanguagesChecker::create($container, [], 'languages_checker', []);
    $this->assertNotNull($checker);
  }

  /**
   * @covers ::checkRequirements
   */
  public function testCheckRequirements() {
    $this->moduleHandler->expects($this->exactly(2))
      ->method('moduleExists')
      ->with('language')
      ->willReturnOnConsecutiveCalls(TRUE, FALSE);
    $this->assertTrue($this->checker->checkRequirements());
    $this->assertFalse($this->checker->checkRequirements());
  }

  /**
   * @covers ::run
   */
  public function testRunMonolingual() {
    $language = $this->createMock(ConfigurableLanguageInterface::class);
    $language->expects($this->exactly(1))
      ->method('getName')
      ->willReturn('Russian');
    $this->languageManager->expects($this->exactly(1))
      ->method('getLanguages')
      ->with(LanguageInterface::STATE_CONFIGURABLE)
      ->willReturn([$language]);
    $this->languageManager->expects($this->exactly(1))
      ->method('isMultilingual')
      ->willReturn(FALSE);

    $results = $this->checker->run();
    $this->assertCount(1, $results);
    $this->assertEquals('error', $results[0]->getStatus());

    $message = new TranslatableMarkup('The site has only one language: %language', ['%language' => 'Russian'], [], $this->getStringTranslationStub());
    $this->assertEquals($message, $results[0]->getMessage());

    $actionMessage = new TranslatableMarkup('Add the desired languages.', [], [], $this->getStringTranslationStub());
    $action = Link::createFromRoute($actionMessage, 'entity.configurable_language.collection');
    $this->assertEquals([$action], $results[0]->getSuggestedActions());
  }

  /**
   * @covers ::run
   */
  public function testRunMultilingual() {
    $language1 = $this->createMock(ConfigurableLanguageInterface::class);
    $language1->expects($this->exactly(1))
      ->method('getName')
      ->willReturn('Catalan');
    $language2 = $this->createMock(ConfigurableLanguageInterface::class);
    $language2->expects($this->exactly(1))
      ->method('getName')
      ->willReturn('Napolitan');

    $this->languageManager->expects($this->exactly(1))
      ->method('getLanguages')
      ->with(LanguageInterface::STATE_CONFIGURABLE)
      ->willReturn([$language1, $language2]);
    $this->languageManager->expects($this->exactly(1))
      ->method('isMultilingual')
      ->willReturn(TRUE);

    $results = $this->checker->run();
    $this->assertCount(1, $results);
    $this->assertEquals('ok', $results[0]->getStatus());

    $message = new TranslatableMarkup('The site has various languages: %languages', ['%languages' => 'Catalan, Napolitan'], [], $this->getStringTranslationStub());
    $this->assertEquals($message, $results[0]->getMessage());
    $this->assertEmpty($results[0]->getSuggestedActions());
  }

  /**
   * @covers ::run
   */
  public function testRunMultilingualWithLingotekEnabled() {
    $languageStorage = $this->createMock(EntityStorageInterface::class);
    $this->entityTypeManager->expects($this->any())
      ->method('getStorage')
      ->with('configurable_language')
      ->willReturn($languageStorage);

    $this->moduleHandler->expects($this->exactly(1))
      ->method('moduleExists')
      ->with('lingotek')
      ->willReturn(TRUE);

    $language1 = $this->createMock(ConfigurableLanguageInterface::class);
    $language1->expects($this->any())
      ->method('getName')
      ->willReturn('Catalan');
    $language1->expects($this->any())
      ->method('getId')
      ->willReturn('ca');
    $language1->expects($this->any())
      ->method('getThirdPartySetting')
      ->with('lingotek', 'locale', NULL)
      ->willReturn('ca_ES');

    $language2 = $this->createMock(ConfigurableLanguageInterface::class);
    $language2->expects($this->any())
      ->method('getName')
      ->willReturn('Napolitan');
    $language2->expects($this->any())
      ->method('getId')
      ->willReturn('nap');
    $language2->expects($this->any())
      ->method('getThirdPartySetting')
      ->with('lingotek', 'locale', NULL)
      ->willReturn(NULL);

    $this->languageLocaleMapper->expects($this->exactly(2))
      ->method('getLocaleForLangcode')
      ->withConsecutive(['ca'], ['nap'])
      ->willReturnOnConsecutiveCalls('ca_ES', 'nap_IT');

    $this->languageManager->expects($this->exactly(1))
      ->method('getLanguages')
      ->with(LanguageInterface::STATE_CONFIGURABLE)
      ->willReturn([$language1, $language2]);
    $this->languageManager->expects($this->exactly(1))
      ->method('isMultilingual')
      ->willReturn(TRUE);

    $languageStorage->expects($this->exactly(2))
      ->method('load')
      ->withConsecutive(['ca'], ['nap'])
      ->willReturnOnConsecutiveCalls($language1, $language2);

    $results = $this->checker->run();
    $this->assertCount(3, $results);
    $this->assertEquals('ok', $results[0]->getStatus());

    $message = new TranslatableMarkup('The site has various languages: %languages', ['%languages' => 'Catalan, Napolitan'], [], $this->getStringTranslationStub());
    $this->assertEquals($message, $results[0]->getMessage());
    $this->assertEmpty($results[0]->getSuggestedActions());

    // Language with explicit locale.
    $this->assertEquals('info', $results[1]->getStatus());
    $message = new TranslatableMarkup('The language %language maps to the %locale Lingotek locale.', [
      '%language' => 'Catalan',
      '%locale' => 'ca_ES',
    ], [], $this->getStringTranslationStub());
    $this->assertEquals($message, $results[1]->getMessage());
    $this->assertEmpty($results[1]->getSuggestedActions());

    // Language without explicit locale.
    $this->assertEquals('warning', $results[2]->getStatus());
    $message = new TranslatableMarkup("The language %language maps to the %locale Lingotek locale, but it's not set explicitly.", [
      '%language' => 'Napolitan',
      '%locale' => 'nap_IT',
    ], [], $this->getStringTranslationStub());
    $this->assertEquals($message, $results[2]->getMessage());
    $actionMessage = new TranslatableMarkup('Verify this is the desired Lingotek locale.', [], [], $this->getStringTranslationStub());
    $action = Link::createFromRoute($actionMessage, 'entity.configurable_language.edit_form', ['configurable_language' => 'nap']);
    $this->assertEquals([$action], $results[2]->getSuggestedActions());
  }

}
