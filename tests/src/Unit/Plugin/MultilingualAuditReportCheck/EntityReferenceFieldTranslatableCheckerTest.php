<?php

namespace Drupal\Tests\multilingual_audit\Unit\Plugin\MultilingualAuditReportCheck;

use Drupal\content_translation\ContentTranslationManagerInterface;
use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldConfigInterface;
use Drupal\Core\Link;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\lingotek\LingotekConfigurationServiceInterface;
use Drupal\multilingual_audit\Plugin\MultilingualAuditReportCheck\EntityReferenceFieldTranslatableChecker;
use Drupal\Tests\UnitTestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Unit test for the entity reference field translatable checker plugin.
 *
 * @coversDefaultClass \Drupal\multilingual_audit\Plugin\MultilingualAuditReportCheck\EntityReferenceFieldTranslatableChecker
 * @group multilingual_audit
 * @preserveGlobalState disabled
 */
class EntityReferenceFieldTranslatableCheckerTest extends UnitTestCase {

  /**
   * The class instance under test.
   *
   * @var \Drupal\multilingual_audit\Plugin\MultilingualAuditReportCheck\EntityReferenceFieldTranslatableChecker
   */
  protected $checker;

  /**
   * The mocked module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $moduleHandler;

  /**
   * The mocked entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $entityTypeManager;

  /**
   * The mocked entity type bundle info.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $entityTypeBundleInfo;

  /**
   * The mocked entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $entityFieldManager;

  /**
   * The mocked content translation manager.
   *
   * @var \Drupal\content_translation\ContentTranslationManagerInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $contentTranslationManager;

  /**
   * The Lingotek configuration service.
   *
   * @var \Drupal\lingotek\LingotekConfigurationServiceInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $lingotekConfiguration;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->moduleHandler = $this->createMock(ModuleHandlerInterface::class);
    $this->entityTypeManager = $this->createMock(EntityTypeManagerInterface::class);
    $this->entityTypeBundleInfo = $this->createMock(EntityTypeBundleInfoInterface::class);
    $this->entityFieldManager = $this->createMock(EntityFieldManagerInterface::class);
    $this->contentTranslationManager = $this->createMock(ContentTranslationManagerInterface::class);
    $this->lingotekConfiguration = $this->createMock(LingotekConfigurationServiceInterface::class);

    $this->checker = new EntityReferenceFieldTranslatableChecker([], 'entity_reference_field_translatable_checker', [], $this->moduleHandler, $this->entityTypeManager, $this->entityTypeBundleInfo, $this->entityFieldManager, $this->contentTranslationManager, $this->lingotekConfiguration);
    $this->checker->setStringTranslation($this->getStringTranslationStub());
  }

  /**
   * @covers ::__construct
   */
  public function testConstruct() {
    $checker = new EntityReferenceFieldTranslatableChecker([], 'entity_reference_field_translatable_checker', [], $this->moduleHandler, $this->entityTypeManager, $this->entityTypeBundleInfo, $this->entityFieldManager, $this->contentTranslationManager);
    $this->assertNotNull($checker);
  }

  /**
   * @covers ::create
   */
  public function testCreate() {
    $container = $this->createMock(ContainerInterface::class);
    $container->expects($this->exactly(2))
      ->method('has')
      ->withConsecutive(['content_translation.manager'], ['lingotek.configuration'])
      ->willReturn(TRUE);
    $container->expects($this->exactly(6))
      ->method('get')
      ->withConsecutive(['content_translation.manager'], ['lingotek.configuration'], ['module_handler'], ['entity_type.manager'], ['entity_type.bundle.info'], ['entity_field.manager'])
      ->willReturnOnConsecutiveCalls($this->contentTranslationManager, $this->lingotekConfiguration, $this->moduleHandler, $this->entityTypeManager, $this->entityTypeBundleInfo, $this->entityFieldManager);
    $checker = EntityReferenceFieldTranslatableChecker::create($container, [], 'entity_reference_field_translatable_checker', []);
    $this->assertNotNull($checker);
  }

  /**
   * @covers ::create
   */
  public function testCreateWithoutContentTranslation() {
    $container = $this->createMock(ContainerInterface::class);
    $container->expects($this->exactly(2))
      ->method('has')
      ->withConsecutive(['content_translation.manager'], ['lingotek.configuration'])
      ->willReturn(FALSE);
    $container->expects($this->exactly(4))
      ->method('get')
      ->withConsecutive(['module_handler'], ['entity_type.manager'], ['entity_type.bundle.info'], ['entity_field.manager'])
      ->willReturnOnConsecutiveCalls($this->moduleHandler, $this->entityTypeManager, $this->entityTypeBundleInfo, $this->entityFieldManager);
    $checker = EntityReferenceFieldTranslatableChecker::create($container, [], 'entity_reference_field_translatable_checker', []);
    $this->assertNotNull($checker);
  }

  /**
   * @covers ::create
   */
  public function testCreateWithoutLingotek() {
    $container = $this->createMock(ContainerInterface::class);
    $container->expects($this->exactly(2))
      ->method('has')
      ->withConsecutive(['content_translation.manager'], ['lingotek.configuration'])
      ->willReturnOnConsecutiveCalls(TRUE, FALSE);
    $container->expects($this->exactly(5))
      ->method('get')
      ->withConsecutive(['content_translation.manager'], ['module_handler'], ['entity_type.manager'], ['entity_type.bundle.info'], ['entity_field.manager'])
      ->willReturnOnConsecutiveCalls($this->contentTranslationManager, $this->moduleHandler, $this->entityTypeManager, $this->entityTypeBundleInfo, $this->entityFieldManager);
    $checker = EntityReferenceFieldTranslatableChecker::create($container, [], 'entity_reference_field_translatable_checker', []);
    $this->assertNotNull($checker);
  }

  /**
   * @covers ::checkRequirements
   */
  public function testCheckRequirements() {
    $this->moduleHandler->expects($this->exactly(2))
      ->method('moduleExists')
      ->withConsecutive(['content_translation'], ['content_translation'])
      ->willReturnOnConsecutiveCalls(TRUE, FALSE);
    $this->assertTrue($this->checker->checkRequirements());
    $this->assertFalse($this->checker->checkRequirements());
  }

  /**
   * @covers ::run
   */
  public function testRunWithEntityReferenceFieldDefinitionsWithoutLanguage() {
    $entityType = $this->createMock(ContentEntityTypeInterface::class);
    $entityType->expects($this->once())
      ->method('hasKey')
      ->with('langcode')
      ->willReturn(FALSE);
    $this->entityTypeManager->expects($this->once())
      ->method('getDefinitions')
      ->willReturn(['entity_type' => $entityType]);

    $results = $this->checker->run();
    $this->assertEmpty($results);
  }

  /**
   * @covers ::run
   */
  public function testRunWithSomeEntityReferenceFields() {
    $entityType = $this->createMock(ContentEntityTypeInterface::class);
    $entityType->expects($this->any())
      ->method('hasKey')
      ->with('langcode')
      ->willReturn(TRUE);
    $entityType->expects($this->any())
      ->method('id')
      ->willReturn('entity_id');
    $entityType->expects($this->any())
      ->method('getBundleEntityType')
      ->willReturn('entity_id');
    $entityType->expects($this->any())
      ->method('getLabel')
      ->willReturn('Entity');

    $this->entityTypeManager->expects($this->once())
      ->method('getDefinitions')
      ->willReturn(['entity_id' => $entityType, 'entity_non_translatable' => $entityType]);
    $this->contentTranslationManager->expects($this->exactly(2))
      ->method('isSupported')
      ->withConsecutive(['entity_id'], ['entity_non_translatable'])
      ->willReturnOnConsecutiveCalls(TRUE, FALSE);
    $this->contentTranslationManager->expects($this->exactly(2))
      ->method('isEnabled')
      ->withConsecutive(['entity_id', 'entity_type_id'], ['entity_id', 'bundle_non_translatable'])
      ->willReturnOnConsecutiveCalls(TRUE, FALSE);
    $this->entityTypeBundleInfo->expects($this->once())
      ->method('getBundleInfo')
      ->with('entity_id')
      ->willReturn([
        'entity_type_id' => [
          'label' => 'Entity Type',
        ],
        'bundle_non_translatable' => [
          'label' => 'Entity Type Non Translatable',
        ],
      ]);

    $this->moduleHandler->expects($this->any())
      ->method('moduleExists')
      ->with('lingotek')
      ->willReturn(FALSE);

    $fieldNonTranslatableDefinition = $this->createMock(FieldConfigInterface::class);
    $fieldNonTranslatableDefinition->expects($this->any())
      ->method('id')
      ->willReturn('other_field');
    $fieldNonTranslatableDefinition->expects($this->any())
      ->method('getType')
      ->willReturn('other_type_field');
    $fieldNonTranslatableDefinition->expects($this->any())
      ->method('isTranslatable')
      ->willReturn(FALSE);
    $fieldNonTranslatableDefinition->expects($this->any())
      ->method('isReadOnly')
      ->willReturn(FALSE);

    $fieldNonTranslatableDefinition->expects($this->any())
      ->method('getName')
      ->willReturn('Other Type Field');

    $baseFieldTranslatableERDefinition = $this->createMock(BaseFieldDefinition::class);
    $baseFieldTranslatableERDefinition->expects($this->any())
      ->method('getType')
      ->willReturn('entity_reference');
    $baseFieldTranslatableERDefinition->expects($this->any())
      ->method('isTranslatable')
      ->willReturn(TRUE);
    $baseFieldTranslatableERDefinition->expects($this->any())
      ->method('isReadOnly')
      ->willReturn(FALSE);
    $baseFieldTranslatableERDefinition->expects($this->any())
      ->method('getName')
      ->willReturn('Base Field ER Translatable');

    $baseFieldReadOnlyTranslatableERDefinition = $this->createMock(BaseFieldDefinition::class);
    $baseFieldReadOnlyTranslatableERDefinition->expects($this->any())
      ->method('getType')
      ->willReturn('entity_reference');
    $baseFieldReadOnlyTranslatableERDefinition->expects($this->any())
      ->method('isTranslatable')
      ->willReturn(TRUE);
    $baseFieldReadOnlyTranslatableERDefinition->expects($this->any())
      ->method('isReadOnly')
      ->willReturn(TRUE);
    $baseFieldReadOnlyTranslatableERDefinition->expects($this->any())
      ->method('getName')
      ->willReturn('Base Field Read-only ER Translatable');

    $fieldTranslatableERDefinition = $this->createMock(FieldConfigInterface::class);
    $fieldTranslatableERDefinition->expects($this->any())
      ->method('id')
      ->willReturn('er_translatable');
    $fieldTranslatableERDefinition->expects($this->any())
      ->method('getType')
      ->willReturn('entity_reference');
    $fieldTranslatableERDefinition->expects($this->any())
      ->method('isTranslatable')
      ->willReturn(TRUE);
    $fieldTranslatableERDefinition->expects($this->any())
      ->method('isReadOnly')
      ->willReturn(FALSE);
    $fieldTranslatableERDefinition->expects($this->any())
      ->method('getName')
      ->willReturn('ER Translatable');

    $fieldNonTranslatableERDefinition = $this->createMock(FieldConfigInterface::class);
    $fieldNonTranslatableERDefinition->expects($this->any())
      ->method('id')
      ->willReturn('er_non_translatable');
    $fieldNonTranslatableERDefinition->expects($this->any())
      ->method('getType')
      ->willReturn('entity_reference');
    $fieldNonTranslatableERDefinition->expects($this->any())
      ->method('isTranslatable')
      ->willReturn(FALSE);
    $fieldNonTranslatableERDefinition->expects($this->any())
      ->method('isReadOnly')
      ->willReturn(FALSE);
    $fieldNonTranslatableERDefinition->expects($this->any())
      ->method('getName')
      ->willReturn('ER Non Translatable');

    $this->entityFieldManager->expects($this->once())
      ->method('getFieldDefinitions')
      ->with('entity_id', 'entity_type_id')
      ->willReturn([
        'base_readonly_er_translatable' => $baseFieldReadOnlyTranslatableERDefinition,
        'base_er_translatable' => $baseFieldTranslatableERDefinition,
        'other_field' => $fieldNonTranslatableDefinition,
        'er_translatable' => $fieldTranslatableERDefinition,
        'er_non_translatable' => $fieldNonTranslatableERDefinition,
      ]);

    $results = $this->checker->run();
    $this->assertCount(3, $results);

    $this->assertEquals('warning', $results[0]->getStatus());
    $message = new TranslatableMarkup('The entity %entity bundle %bundle entity_reference field %field is enabled for content translation. That means you can reference different entities in each translation.', [
      '%entity' => 'Entity',
      '%bundle' => 'Entity Type',
      '%field' => 'Base Field ER Translatable',
    ], [], $this->getStringTranslationStub());
    $this->assertEquals($message, $results[0]->getMessage());
    $actionMessage = new TranslatableMarkup('Review the translatability of this field.', [
      '%entity' => 'Entity',
      '%bundle' => 'Entity Type',
      '%field' => 'Base Field ER Translatable',
    ], [], $this->getStringTranslationStub());
    $action = Link::createFromRoute($actionMessage, 'language.content_settings_page');
    $this->assertEquals([$action], $results[0]->getSuggestedActions());

    $this->assertEquals('warning', $results[1]->getStatus());
    $message = new TranslatableMarkup('The entity %entity bundle %bundle entity_reference field %field is enabled for content translation. That means you can reference different entities in each translation.', [
      '%entity' => 'Entity',
      '%bundle' => 'Entity Type',
      '%field' => 'ER Translatable',
    ], [], $this->getStringTranslationStub());
    $this->assertEquals($message, $results[1]->getMessage());
    $actionMessage = new TranslatableMarkup('Review the translatability of this field.', [
      '%entity' => 'Entity',
      '%bundle' => 'Entity Type',
      '%field' => 'ER Translatable',
    ], [], $this->getStringTranslationStub());
    $action = Link::createFromRoute($actionMessage, 'entity.field_config.entity_id_field_edit_form', [
      'entity_id' => 'entity_type_id',
      'field_config' => 'er_translatable',
    ]);
    $this->assertEquals([$action], $results[1]->getSuggestedActions());

    $this->assertEquals('warning', $results[2]->getStatus());
    $message = new TranslatableMarkup('The entity %entity bundle %bundle entity_reference field %field is not enabled for content translation. That means the field will reference the same entities in each translation.', [
      '%entity' => 'Entity',
      '%bundle' => 'Entity Type',
      '%field' => 'ER Non Translatable',
    ], [], $this->getStringTranslationStub());
    $this->assertEquals($message, $results[2]->getMessage());
    $actionMessage = new TranslatableMarkup('Review the translatability of this field.', [
      '%entity' => 'Entity',
      '%bundle' => 'Entity Type',
      '%field' => 'ER Non Translatable',
    ], [], $this->getStringTranslationStub());
    $action = Link::createFromRoute($actionMessage, 'entity.field_config.entity_id_field_edit_form', [
      'entity_id' => 'entity_type_id',
      'field_config' => 'er_non_translatable',
    ]);
    $this->assertEquals([$action], $results[2]->getSuggestedActions());
  }

  /**
   * @covers ::run
   */
  public function testRunWithSomeEntityReferenceFieldsWithLingotek() {
    $entityType = $this->createMock(ContentEntityTypeInterface::class);
    $entityType->expects($this->any())
      ->method('hasKey')
      ->with('langcode')
      ->willReturn(TRUE);
    $entityType->expects($this->any())
      ->method('id')
      ->willReturn('entity_id');
    $entityType->expects($this->any())
      ->method('getBundleEntityType')
      ->willReturn('entity_id');
    $entityType->expects($this->any())
      ->method('getLabel')
      ->willReturn('Entity');

    $this->entityTypeManager->expects($this->once())
      ->method('getDefinitions')
      ->willReturn(['entity_id' => $entityType, 'entity_non_translatable' => $entityType]);
    $this->contentTranslationManager->expects($this->exactly(2))
      ->method('isSupported')
      ->withConsecutive(['entity_id'], ['entity_non_translatable'])
      ->willReturnOnConsecutiveCalls(TRUE, FALSE);
    $this->contentTranslationManager->expects($this->exactly(2))
      ->method('isEnabled')
      ->withConsecutive(['entity_id', 'entity_type_id'], ['entity_id', 'bundle_non_translatable'])
      ->willReturnOnConsecutiveCalls(TRUE, FALSE);
    $this->entityTypeBundleInfo->expects($this->once())
      ->method('getBundleInfo')
      ->with('entity_id')
      ->willReturn([
        'entity_type_id' => [
          'label' => 'Entity Type',
        ],
        'bundle_non_translatable' => [
          'label' => 'Entity Type Non Translatable',
        ],
      ]);

    $this->moduleHandler->expects($this->any())
      ->method('moduleExists')
      ->with('lingotek')
      ->willReturn(TRUE);

    $this->lingotekConfiguration->expects($this->exactly(2))
      ->method('isEnabled')
      ->withConsecutive(['entity_id', 'entity_type_id'], ['entity_id', 'entity_type_id'])
      ->willReturnOnConsecutiveCalls(TRUE, TRUE);
    $this->lingotekConfiguration->expects($this->exactly(2))
      ->method('isFieldLingotekEnabled')
      ->withConsecutive(['entity_id', 'entity_type_id', 'base_er_non_translatable'], ['entity_id', 'entity_type_id', 'er_translatable'])
      ->willReturnOnConsecutiveCalls(TRUE, TRUE);

    $baseFieldReadOnlyTranslatableERDefinition = $this->createMock(BaseFieldDefinition::class);
    $baseFieldReadOnlyTranslatableERDefinition->expects($this->any())
      ->method('getType')
      ->willReturn('entity_reference');
    $baseFieldReadOnlyTranslatableERDefinition->expects($this->any())
      ->method('isTranslatable')
      ->willReturn(TRUE);
    $baseFieldReadOnlyTranslatableERDefinition->expects($this->any())
      ->method('isReadOnly')
      ->willReturn(TRUE);
    $baseFieldReadOnlyTranslatableERDefinition->expects($this->any())
      ->method('getName')
      ->willReturn('Base Field Read-only ER Translatable');

    $fieldNonTranslatableDefinition = $this->createMock(FieldConfigInterface::class);
    $fieldNonTranslatableDefinition->expects($this->any())
      ->method('id')
      ->willReturn('other_field');
    $fieldNonTranslatableDefinition->expects($this->any())
      ->method('getType')
      ->willReturn('other_type_field');
    $fieldNonTranslatableDefinition->expects($this->any())
      ->method('isTranslatable')
      ->willReturn(FALSE);
    $fieldNonTranslatableDefinition->expects($this->any())
      ->method('getName')
      ->willReturn('Other Type Field');

    $baseFieldTranslatableERDefinition = $this->createMock(BaseFieldDefinition::class);
    $baseFieldTranslatableERDefinition->expects($this->any())
      ->method('getType')
      ->willReturn('entity_reference');
    $baseFieldTranslatableERDefinition->expects($this->any())
      ->method('isTranslatable')
      ->willReturn(TRUE);
    $baseFieldTranslatableERDefinition->expects($this->any())
      ->method('getName')
      ->willReturn('Base Field ER Translatable');

    $fieldTranslatableERDefinition = $this->createMock(FieldConfigInterface::class);
    $fieldTranslatableERDefinition->expects($this->any())
      ->method('id')
      ->willReturn('er_translatable');
    $fieldTranslatableERDefinition->expects($this->any())
      ->method('getType')
      ->willReturn('entity_reference');
    $fieldTranslatableERDefinition->expects($this->any())
      ->method('isTranslatable')
      ->willReturn(TRUE);
    $fieldTranslatableERDefinition->expects($this->any())
      ->method('getName')
      ->willReturn('ER Translatable');

    $fieldNonTranslatableERDefinition = $this->createMock(FieldConfigInterface::class);
    $fieldNonTranslatableERDefinition->expects($this->any())
      ->method('id')
      ->willReturn('er_non_translatable');
    $fieldNonTranslatableERDefinition->expects($this->any())
      ->method('getType')
      ->willReturn('entity_reference');
    $fieldNonTranslatableERDefinition->expects($this->any())
      ->method('isTranslatable')
      ->willReturn(FALSE);
    $fieldNonTranslatableERDefinition->expects($this->any())
      ->method('getName')
      ->willReturn('ER Non Translatable');

    $this->entityFieldManager->expects($this->once())
      ->method('getFieldDefinitions')
      ->with('entity_id', 'entity_type_id')
      ->willReturn([
        'base_readonly_er_translatable' => $baseFieldReadOnlyTranslatableERDefinition,
        'base_er_non_translatable' => $baseFieldTranslatableERDefinition,
        'other_field' => $fieldNonTranslatableDefinition,
        'er_translatable' => $fieldTranslatableERDefinition,
        'er_non_translatable' => $fieldNonTranslatableERDefinition,
      ]);

    $results = $this->checker->run();
    $this->assertCount(5, $results);

    $this->assertEquals('warning', $results[0]->getStatus());
    $message = new TranslatableMarkup('The entity %entity bundle %bundle entity_reference field %field is enabled for content translation. That means you can reference different entities in each translation.', [
      '%entity' => 'Entity',
      '%bundle' => 'Entity Type',
      '%field' => 'Base Field ER Translatable',
    ], [], $this->getStringTranslationStub());
    $this->assertEquals($message, $results[0]->getMessage());
    $actionMessage = new TranslatableMarkup('Review the translatability of this field.', [
      '%entity' => 'Entity',
      '%bundle' => 'Entity Type',
      '%field' => 'Base Field ER Translatable',
    ], [], $this->getStringTranslationStub());
    $action = Link::createFromRoute($actionMessage, 'language.content_settings_page');
    $this->assertEquals([$action], $results[0]->getSuggestedActions());

    $this->assertEquals('warning', $results[1]->getStatus());
    $message = new TranslatableMarkup('The entity %entity bundle %bundle entity_reference field %field is enabled for Lingotek translation. That means the referenced content will be embedded with the parent document. You may only want that if this content is not used elsewhere.', [
      '%entity' => 'Entity',
      '%bundle' => 'Entity Type',
      '%field' => 'Base Field ER Translatable',
    ], [], $this->getStringTranslationStub());
    $this->assertEquals($message, $results[1]->getMessage());
    $action = Link::createFromRoute('Review Lingotek translatability of this field.', 'lingotek.settings.content_form', [
      'entity_type' => 'entity_id',
      'bundle' => 'entity_type_id',
    ]);
    $this->assertEquals([$action], $results[1]->getSuggestedActions());

    $this->assertEquals('warning', $results[2]->getStatus());
    $message = new TranslatableMarkup('The entity %entity bundle %bundle entity_reference field %field is enabled for content translation. That means you can reference different entities in each translation.', [
      '%entity' => 'Entity',
      '%bundle' => 'Entity Type',
      '%field' => 'ER Translatable',
    ], [], $this->getStringTranslationStub());
    $this->assertEquals($message, $results[2]->getMessage());
    $actionMessage = new TranslatableMarkup('Review the translatability of this field.', [
      '%entity' => 'Entity',
      '%bundle' => 'Entity Type',
      '%field' => 'ER Translatable',
    ], [], $this->getStringTranslationStub());
    $action = Link::createFromRoute($actionMessage, 'entity.field_config.entity_id_field_edit_form', [
      'entity_id' => 'entity_type_id',
      'field_config' => 'er_translatable',
    ]);
    $this->assertEquals([$action], $results[2]->getSuggestedActions());

    $this->assertEquals('warning', $results[3]->getStatus());
    $message = new TranslatableMarkup('The entity %entity bundle %bundle entity_reference field %field is enabled for Lingotek translation. That means the referenced content will be embedded with the parent document. You may only want that if this content is not used elsewhere.', [
      '%entity' => 'Entity',
      '%bundle' => 'Entity Type',
      '%field' => 'ER Translatable',
    ], [], $this->getStringTranslationStub());
    $this->assertEquals($message, $results[3]->getMessage());
    $action = Link::createFromRoute('Review Lingotek translatability of this field.', 'lingotek.settings.content_form', [
      'entity_type' => 'entity_id',
      'bundle' => 'entity_type_id',
    ]);
    $this->assertEquals([$action], $results[3]->getSuggestedActions());

    $this->assertEquals('warning', $results[4]->getStatus());
    $message = new TranslatableMarkup('The entity %entity bundle %bundle entity_reference field %field is not enabled for content translation. That means the field will reference the same entities in each translation.', [
      '%entity' => 'Entity',
      '%bundle' => 'Entity Type',
      '%field' => 'ER Non Translatable',
    ], [], $this->getStringTranslationStub());
    $this->assertEquals($message, $results[4]->getMessage());
    $actionMessage = new TranslatableMarkup('Review the translatability of this field.', [
      '%entity' => 'Entity',
      '%bundle' => 'Entity Type',
      '%field' => 'ER Non Translatable',
    ], [], $this->getStringTranslationStub());
    $action = Link::createFromRoute($actionMessage, 'entity.field_config.entity_id_field_edit_form', [
      'entity_id' => 'entity_type_id',
      'field_config' => 'er_non_translatable',
    ]);
    $this->assertEquals([$action], $results[4]->getSuggestedActions());
  }

}
