<?php

namespace Drupal\Tests\multilingual_audit\Unit\Plugin\MultilingualAuditReportCheck;

use Drupal\content_translation\ContentTranslationManagerInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Link;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\lingotek\LingotekConfigurationServiceInterface;
use Drupal\multilingual_audit\Plugin\MultilingualAuditReportCheck\ParagraphsTranslatableChecker;
use Drupal\paragraphs\Entity\ParagraphsType;
use Drupal\Tests\UnitTestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Unit test for the paragraphs translatable checker plugin.
 *
 * @coversDefaultClass \Drupal\multilingual_audit\Plugin\MultilingualAuditReportCheck\ParagraphsTranslatableChecker
 * @group multilingual_audit
 * @preserveGlobalState disabled
 */
class ParagraphsTranslatableCheckerTest extends UnitTestCase {

  /**
   * The object under test.
   *
   * @var \Drupal\multilingual_audit\Plugin\MultilingualAuditReportCheck\ParagraphsTranslatableChecker
   */
  protected $checker;

  /**
   * The mocked module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $moduleHandler;

  /**
   * The mocked entity type bundle info.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $entityTypeBundleInfo;

  /**
   * The mocked entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $entityFieldManager;

  /**
   * The mocked content translation manager.
   *
   * @var \Drupal\content_translation\ContentTranslationManagerInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $contentTranslationManager;

  /**
   * The Lingotek configuration service.
   *
   * @var \Drupal\lingotek\LingotekConfigurationServiceInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $lingotekConfiguration;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->moduleHandler = $this->createMock(ModuleHandlerInterface::class);
    $this->entityTypeManager = $this->createMock(EntityTypeManagerInterface::class);
    $this->contentTranslationManager = $this->createMock(ContentTranslationManagerInterface::class);
    $this->lingotekConfiguration = $this->createMock(LingotekConfigurationServiceInterface::class);

    $this->checker = new ParagraphsTranslatableChecker([], 'paragraphs_translatable_checker', [], $this->moduleHandler, $this->entityTypeManager, $this->contentTranslationManager, $this->lingotekConfiguration);
    $this->checker->setStringTranslation($this->getStringTranslationStub());
  }

  /**
   * @covers ::__construct
   */
  public function testConstruct() {
    $checker = new ParagraphsTranslatableChecker([], 'paragraphs_translatable_checker', [], $this->moduleHandler, $this->entityTypeManager, $this->contentTranslationManager);
    $this->assertNotNull($checker);
  }

  /**
   * @covers ::create
   */
  public function testCreate() {
    $container = $this->createMock(ContainerInterface::class);
    $container->expects($this->exactly(2))
      ->method('has')
      ->withConsecutive(['content_translation.manager'], ['lingotek.configuration'])
      ->willReturn(TRUE);
    $container->expects($this->exactly(4))
      ->method('get')
      ->withConsecutive(['content_translation.manager'], ['lingotek.configuration'], ['module_handler'], ['entity_type.manager'])
      ->willReturnOnConsecutiveCalls($this->contentTranslationManager, $this->lingotekConfiguration, $this->moduleHandler, $this->entityTypeManager);
    $checker = ParagraphsTranslatableChecker::create($container, [], 'paragraphs_translatable_checker', []);
    $this->assertNotNull($checker);
  }

  /**
   * @covers ::create
   */
  public function testCreateWithoutContentTranslation() {
    $container = $this->createMock(ContainerInterface::class);
    $container->expects($this->exactly(2))
      ->method('has')
      ->withConsecutive(['content_translation.manager'], ['lingotek.configuration'])
      ->willReturn(FALSE);
    $container->expects($this->exactly(2))
      ->method('get')
      ->withConsecutive(['module_handler'], ['entity_type.manager'])
      ->willReturnOnConsecutiveCalls($this->moduleHandler, $this->entityTypeManager);
    $checker = ParagraphsTranslatableChecker::create($container, [], 'paragraphs_translatable_checker', []);
    $this->assertNotNull($checker);
  }

  /**
   * @covers ::create
   */
  public function testCreateWithoutLingotek() {
    $container = $this->createMock(ContainerInterface::class);
    $container->expects($this->exactly(2))
      ->method('has')
      ->withConsecutive(['content_translation.manager'], ['lingotek.configuration'])
      ->willReturnOnConsecutiveCalls(TRUE, FALSE);
    $container->expects($this->exactly(3))
      ->method('get')
      ->withConsecutive(['content_translation.manager'], ['module_handler'], ['entity_type.manager'])
      ->willReturnOnConsecutiveCalls($this->contentTranslationManager, $this->moduleHandler, $this->entityTypeManager);
    $checker = ParagraphsTranslatableChecker::create($container, [], 'paragraphs_translatable_checker', []);
    $this->assertNotNull($checker);
  }

  /**
   * @covers ::checkRequirements
   */
  public function testCheckRequirements() {
    $this->moduleHandler->expects($this->exactly(4))
      ->method('moduleExists')
      ->withConsecutive(['content_translation'], ['paragraphs'], ['content_translation'], ['paragraphs'])
      ->willReturnOnConsecutiveCalls(TRUE, TRUE, TRUE, FALSE);
    $this->assertTrue($this->checker->checkRequirements());
    $this->assertFalse($this->checker->checkRequirements());
  }

  /**
   * @covers ::run
   */
  public function testRunWithNoParagraphTypes() {
    $entityStorage = $this->createMock(EntityStorageInterface::class);
    $this->entityTypeManager->expects($this->once())
      ->method('getStorage')
      ->with('paragraphs_type')
      ->willReturn($entityStorage);

    $results = $this->checker->run();
    $this->assertEmpty($results);
  }

  /**
   * @covers ::run
   */
  public function testRunWithSomeParagraphTypes() {
    $paragraphsType1 = $this->createMock(ParagraphsType::class);
    $paragraphsType1->expects($this->any())
      ->method('id')
      ->willReturn('paragraph_type_one');
    $paragraphsType1->expects($this->any())
      ->method('label')
      ->willReturn('Paragraph Type One');

    $paragraphsType2 = $this->createMock(ParagraphsType::class);
    $paragraphsType2->expects($this->any())
      ->method('id')
      ->willReturn('paragraph_type_two');
    $paragraphsType2->expects($this->any())
      ->method('label')
      ->willReturn('Paragraph Type Two');

    $entityStorage = $this->createMock(EntityStorageInterface::class);
    $this->entityTypeManager->expects($this->once())
      ->method('getStorage')
      ->with('paragraphs_type')
      ->willReturn($entityStorage);
    $entityStorage->expects($this->once())
      ->method('loadMultiple')
      ->willReturn([$paragraphsType1, $paragraphsType2]);

    $this->contentTranslationManager->expects($this->exactly(2))
      ->method('isEnabled')
      ->withConsecutive(['paragraph', 'paragraph_type_one'], ['paragraph', 'paragraph_type_two'])
      ->willReturnOnConsecutiveCalls(FALSE, TRUE);

    $results = $this->checker->run();
    $this->assertCount(2, $results);

    $this->assertEquals('warning', $results[0]->getStatus());
    $message = new TranslatableMarkup('The paragraph type %paragraphType is not enabled for content translation.', ['%paragraphType' => 'Paragraph Type One'], [], $this->getStringTranslationStub());
    $this->assertEquals($message, $results[0]->getMessage());
    $actionMessage = new TranslatableMarkup('Enable content translatability if desired.', [], [], $this->getStringTranslationStub());
    $action = Link::createFromRoute($actionMessage, 'language.content_settings_page');
    $this->assertEquals([$action], $results[0]->getSuggestedActions());

    $this->assertEquals('ok', $results[1]->getStatus());
    $message = new TranslatableMarkup('The paragraph type %paragraphType is enabled for content translation.', ['%paragraphType' => 'Paragraph Type Two'], [], $this->getStringTranslationStub());
    $this->assertEquals($message, $results[1]->getMessage());
    $this->assertEmpty($results[1]->getSuggestedActions());
  }

  /**
   * @covers ::run
   */
  public function testRunWithSomeParagraphTypesAndLingotek() {
    $this->moduleHandler->expects($this->any())
      ->method('moduleExists')
      ->with('lingotek')
      ->willReturn(TRUE);

    $paragraphsType1 = $this->createMock(ParagraphsType::class);
    $paragraphsType1->expects($this->any())
      ->method('id')
      ->willReturn('paragraph_type_one');
    $paragraphsType1->expects($this->any())
      ->method('label')
      ->willReturn('Paragraph Type One');

    $paragraphsType2 = $this->createMock(ParagraphsType::class);
    $paragraphsType2->expects($this->any())
      ->method('id')
      ->willReturn('paragraph_type_two');
    $paragraphsType2->expects($this->any())
      ->method('label')
      ->willReturn('Paragraph Type Two');

    $entityStorage = $this->createMock(EntityStorageInterface::class);
    $this->entityTypeManager->expects($this->once())
      ->method('getStorage')
      ->with('paragraphs_type')
      ->willReturn($entityStorage);
    $entityStorage->expects($this->once())
      ->method('loadMultiple')
      ->willReturn([$paragraphsType1, $paragraphsType2]);

    $this->contentTranslationManager->expects($this->exactly(2))
      ->method('isEnabled')
      ->withConsecutive(['paragraph', 'paragraph_type_one'], ['paragraph', 'paragraph_type_two'])
      ->willReturnOnConsecutiveCalls(TRUE, TRUE);

    $this->lingotekConfiguration->expects($this->exactly(2))
      ->method('isEnabled')
      ->withConsecutive(['paragraph', 'paragraph_type_one'], ['paragraph', 'paragraph_type_two'])
      ->willReturnOnConsecutiveCalls(TRUE, FALSE);

    $results = $this->checker->run();
    $this->assertCount(2, $results);

    $this->assertEquals('ok', $results[0]->getStatus());
    $message = new TranslatableMarkup('The paragraph type %paragraphType is enabled for content translation.', ['%paragraphType' => 'Paragraph Type One'], [], $this->getStringTranslationStub());
    $this->assertEquals($message, $results[0]->getMessage());
    $this->assertEmpty($results[0]->getSuggestedActions());

    $this->assertEquals('warning', $results[1]->getStatus());
    $message = new TranslatableMarkup('The paragraph type %paragraphType is not enabled for Lingotek translation.', ['%paragraphType' => 'Paragraph Type Two'], [], $this->getStringTranslationStub());
    $this->assertEquals($message, $results[1]->getMessage());
    $actionMessage = new TranslatableMarkup('Enable Lingotek translatability if desired.', [], [], $this->getStringTranslationStub());
    $action = Link::createFromRoute($actionMessage, 'lingotek.settings.content_form', [
      'entity_type' => 'paragraph',
      'bundle' => 'paragraph_type_two',
    ]);
    $this->assertEquals([$action], $results[1]->getSuggestedActions());
  }

}
